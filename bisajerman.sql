/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.1.32-MariaDB : Database - bisajerman
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bisajerman` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `bisajerman`;

/*Table structure for table `acos` */

DROP TABLE IF EXISTS `acos`;

CREATE TABLE `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=208 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `acos` */

insert  into `acos`(`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values 
(1,NULL,'',NULL,'controllers',1,414),
(2,1,'',NULL,'Acl',2,25),
(3,2,'',NULL,'AclActions',3,16),
(4,3,'',NULL,'admin_index',4,5),
(5,3,'',NULL,'admin_add',6,7),
(6,3,'',NULL,'admin_edit',8,9),
(7,3,'',NULL,'admin_delete',10,11),
(8,3,'',NULL,'admin_move',12,13),
(9,3,'',NULL,'admin_generate',14,15),
(10,2,'',NULL,'AclPermissions',17,24),
(11,10,'',NULL,'admin_index',18,19),
(12,10,'',NULL,'admin_toggle',20,21),
(13,10,'',NULL,'admin_upgrade',22,23),
(14,1,'',NULL,'Blocks',26,55),
(15,14,'',NULL,'Blocks',27,44),
(16,15,'',NULL,'admin_toggle',28,29),
(17,15,'',NULL,'admin_index',30,31),
(18,15,'',NULL,'admin_add',32,33),
(19,15,'',NULL,'admin_edit',34,35),
(20,15,'',NULL,'admin_delete',36,37),
(21,15,'',NULL,'admin_moveup',38,39),
(22,15,'',NULL,'admin_movedown',40,41),
(23,15,'',NULL,'admin_process',42,43),
(24,14,'',NULL,'Regions',45,54),
(25,24,'',NULL,'admin_index',46,47),
(26,24,'',NULL,'admin_add',48,49),
(27,24,'',NULL,'admin_edit',50,51),
(28,24,'',NULL,'admin_delete',52,53),
(29,1,'',NULL,'Comments',56,73),
(30,29,'',NULL,'Comments',57,72),
(31,30,'',NULL,'admin_index',58,59),
(32,30,'',NULL,'admin_edit',60,61),
(33,30,'',NULL,'admin_delete',62,63),
(34,30,'',NULL,'admin_process',64,65),
(35,30,'',NULL,'index',66,67),
(36,30,'',NULL,'add',68,69),
(37,30,'',NULL,'delete',70,71),
(38,1,'',NULL,'Contacts',74,97),
(39,38,'',NULL,'Contacts',75,86),
(40,39,'',NULL,'admin_index',76,77),
(41,39,'',NULL,'admin_add',78,79),
(42,39,'',NULL,'admin_edit',80,81),
(43,39,'',NULL,'admin_delete',82,83),
(44,39,'',NULL,'view',84,85),
(45,38,'',NULL,'Messages',87,96),
(46,45,'',NULL,'admin_index',88,89),
(47,45,'',NULL,'admin_edit',90,91),
(48,45,'',NULL,'admin_delete',92,93),
(49,45,'',NULL,'admin_process',94,95),
(50,1,'',NULL,'Croogo',98,99),
(51,1,'',NULL,'Extensions',100,139),
(52,51,'',NULL,'ExtensionsLocales',101,112),
(53,52,'',NULL,'admin_index',102,103),
(54,52,'',NULL,'admin_activate',104,105),
(55,52,'',NULL,'admin_add',106,107),
(56,52,'',NULL,'admin_edit',108,109),
(57,52,'',NULL,'admin_delete',110,111),
(58,51,'',NULL,'ExtensionsPlugins',113,124),
(59,58,'',NULL,'admin_index',114,115),
(60,58,'',NULL,'admin_add',116,117),
(61,58,'',NULL,'admin_delete',118,119),
(62,58,'',NULL,'admin_toggle',120,121),
(63,58,'',NULL,'admin_migrate',122,123),
(64,51,'',NULL,'ExtensionsThemes',125,138),
(65,64,'',NULL,'admin_index',126,127),
(66,64,'',NULL,'admin_activate',128,129),
(67,64,'',NULL,'admin_add',130,131),
(68,64,'',NULL,'admin_editor',132,133),
(69,64,'',NULL,'admin_save',134,135),
(70,64,'',NULL,'admin_delete',136,137),
(71,1,'',NULL,'FileManager',140,175),
(72,71,'',NULL,'Attachments',141,152),
(73,72,'',NULL,'admin_index',142,143),
(74,72,'',NULL,'admin_add',144,145),
(75,72,'',NULL,'admin_edit',146,147),
(76,72,'',NULL,'admin_delete',148,149),
(77,72,'',NULL,'admin_browse',150,151),
(78,71,'',NULL,'FileManager',153,174),
(79,78,'',NULL,'admin_index',154,155),
(80,78,'',NULL,'admin_browse',156,157),
(81,78,'',NULL,'admin_editfile',158,159),
(82,78,'',NULL,'admin_upload',160,161),
(83,78,'',NULL,'admin_delete_file',162,163),
(84,78,'',NULL,'admin_delete_directory',164,165),
(85,78,'',NULL,'admin_rename',166,167),
(86,78,'',NULL,'admin_create_directory',168,169),
(87,78,'',NULL,'admin_create_file',170,171),
(88,78,'',NULL,'admin_chmod',172,173),
(89,1,'',NULL,'Install',176,189),
(90,89,'',NULL,'Install',177,188),
(91,90,'',NULL,'index',178,179),
(92,90,'',NULL,'database',180,181),
(93,90,'',NULL,'data',182,183),
(94,90,'',NULL,'adminuser',184,185),
(95,90,'',NULL,'finish',186,187),
(96,1,'',NULL,'Menus',190,219),
(97,96,'',NULL,'Links',191,208),
(98,97,'',NULL,'admin_toggle',192,193),
(99,97,'',NULL,'admin_index',194,195),
(100,97,'',NULL,'admin_add',196,197),
(101,97,'',NULL,'admin_edit',198,199),
(102,97,'',NULL,'admin_delete',200,201),
(103,97,'',NULL,'admin_moveup',202,203),
(104,97,'',NULL,'admin_movedown',204,205),
(105,97,'',NULL,'admin_process',206,207),
(106,96,'',NULL,'Menus',209,218),
(107,106,'',NULL,'admin_index',210,211),
(108,106,'',NULL,'admin_add',212,213),
(109,106,'',NULL,'admin_edit',214,215),
(110,106,'',NULL,'admin_delete',216,217),
(111,1,'',NULL,'Meta',220,221),
(112,1,'',NULL,'Migrations',222,223),
(113,1,'',NULL,'Nodes',224,257),
(114,113,'',NULL,'Nodes',225,256),
(115,114,'',NULL,'admin_toggle',226,227),
(116,114,'',NULL,'admin_index',228,229),
(117,114,'',NULL,'admin_create',230,231),
(118,114,'',NULL,'admin_add',232,233),
(119,114,'',NULL,'admin_edit',234,235),
(120,114,'',NULL,'admin_update_paths',236,237),
(121,114,'',NULL,'admin_delete',238,239),
(122,114,'',NULL,'admin_delete_meta',240,241),
(123,114,'',NULL,'admin_add_meta',242,243),
(124,114,'',NULL,'admin_process',244,245),
(125,114,'',NULL,'index',246,247),
(126,114,'',NULL,'term',248,249),
(127,114,'',NULL,'promoted',250,251),
(128,114,'',NULL,'search',252,253),
(129,114,'',NULL,'view',254,255),
(130,1,'',NULL,'Search',258,259),
(131,1,'',NULL,'Settings',260,297),
(132,131,'',NULL,'Languages',261,276),
(133,132,'',NULL,'admin_index',262,263),
(134,132,'',NULL,'admin_add',264,265),
(135,132,'',NULL,'admin_edit',266,267),
(136,132,'',NULL,'admin_delete',268,269),
(137,132,'',NULL,'admin_moveup',270,271),
(138,132,'',NULL,'admin_movedown',272,273),
(139,132,'',NULL,'admin_select',274,275),
(140,131,'',NULL,'Settings',277,296),
(141,140,'',NULL,'admin_dashboard',278,279),
(142,140,'',NULL,'admin_index',280,281),
(143,140,'',NULL,'admin_view',282,283),
(144,140,'',NULL,'admin_add',284,285),
(145,140,'',NULL,'admin_edit',286,287),
(146,140,'',NULL,'admin_delete',288,289),
(147,140,'',NULL,'admin_prefix',290,291),
(148,140,'',NULL,'admin_moveup',292,293),
(149,140,'',NULL,'admin_movedown',294,295),
(150,1,'',NULL,'Taxonomy',298,337),
(151,150,'',NULL,'Terms',299,312),
(152,151,'',NULL,'admin_index',300,301),
(153,151,'',NULL,'admin_add',302,303),
(154,151,'',NULL,'admin_edit',304,305),
(155,151,'',NULL,'admin_delete',306,307),
(156,151,'',NULL,'admin_moveup',308,309),
(157,151,'',NULL,'admin_movedown',310,311),
(158,150,'',NULL,'Types',313,322),
(159,158,'',NULL,'admin_index',314,315),
(160,158,'',NULL,'admin_add',316,317),
(161,158,'',NULL,'admin_edit',318,319),
(162,158,'',NULL,'admin_delete',320,321),
(163,150,'',NULL,'Vocabularies',323,336),
(164,163,'',NULL,'admin_index',324,325),
(165,163,'',NULL,'admin_add',326,327),
(166,163,'',NULL,'admin_edit',328,329),
(167,163,'',NULL,'admin_delete',330,331),
(168,163,'',NULL,'admin_moveup',332,333),
(169,163,'',NULL,'admin_movedown',334,335),
(170,1,'',NULL,'Ckeditor',338,339),
(171,1,'',NULL,'Users',340,385),
(172,171,'',NULL,'Roles',341,350),
(173,172,'',NULL,'admin_index',342,343),
(174,172,'',NULL,'admin_add',344,345),
(175,172,'',NULL,'admin_edit',346,347),
(176,172,'',NULL,'admin_delete',348,349),
(177,171,'',NULL,'Users',351,384),
(178,177,'',NULL,'admin_index',352,353),
(179,177,'',NULL,'admin_add',354,355),
(180,177,'',NULL,'admin_edit',356,357),
(181,177,'',NULL,'admin_reset_password',358,359),
(182,177,'',NULL,'admin_delete',360,361),
(183,177,'',NULL,'admin_login',362,363),
(184,177,'',NULL,'admin_logout',364,365),
(185,177,'',NULL,'index',366,367),
(186,177,'',NULL,'add',368,369),
(187,177,'',NULL,'activate',370,371),
(188,177,'',NULL,'edit',372,373),
(189,177,'',NULL,'forgot',374,375),
(190,177,'',NULL,'reset',376,377),
(191,177,'',NULL,'login',378,379),
(192,177,'',NULL,'logout',380,381),
(193,177,'',NULL,'view',382,383),
(194,1,NULL,NULL,'Translate',386,395),
(195,194,NULL,NULL,'Translate',387,394),
(196,195,NULL,NULL,'admin_index',388,389),
(197,195,NULL,NULL,'admin_edit',390,391),
(198,195,NULL,NULL,'admin_delete',392,393),
(199,1,NULL,NULL,'Seo',396,409),
(200,199,NULL,NULL,'admin_index',397,398),
(201,199,NULL,NULL,'admin_dashboard',399,400),
(202,199,NULL,NULL,'admin_twitter',401,402),
(203,199,NULL,NULL,'index',403,404),
(204,199,NULL,NULL,'sitemap',405,406),
(205,199,NULL,NULL,'sitemapxml',407,408),
(206,1,NULL,NULL,'Nodeattachment',410,413),
(207,206,NULL,NULL,'downloadsByTerms',411,412);

/*Table structure for table `aros` */

DROP TABLE IF EXISTS `aros`;

CREATE TABLE `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `aros` */

insert  into `aros`(`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values 
(1,2,'Role',1,'Role-admin',3,6),
(2,3,'Role',2,'Role-registered',2,7),
(3,NULL,'Role',3,'Role-public',1,8),
(4,1,'User',1,'admin',4,5);

/*Table structure for table `aros_acos` */

DROP TABLE IF EXISTS `aros_acos`;

CREATE TABLE `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `_read` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `_update` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `_delete` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `aros_acos` */

insert  into `aros_acos`(`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values 
(1,3,35,'1','1','1','1'),
(2,3,36,'1','1','1','1'),
(3,2,37,'1','1','1','1'),
(4,3,44,'1','1','1','1'),
(5,3,125,'1','1','1','1'),
(6,3,126,'1','1','1','1'),
(7,3,127,'1','1','1','1'),
(8,3,128,'1','1','1','1'),
(9,3,129,'1','1','1','1'),
(10,2,185,'1','1','1','1'),
(11,3,186,'1','1','1','1'),
(12,3,187,'1','1','1','1'),
(13,2,188,'1','1','1','1'),
(14,3,189,'1','1','1','1'),
(15,3,190,'1','1','1','1'),
(16,3,191,'1','1','1','1'),
(17,2,192,'1','1','1','1'),
(18,2,193,'1','1','1','1'),
(19,3,183,'1','1','1','1'),
(20,1,200,'1','1','1','1'),
(21,1,201,'1','1','1','1'),
(22,1,202,'1','1','1','1'),
(23,1,203,'1','1','1','1'),
(24,3,203,'1','1','1','1'),
(25,2,203,'1','1','1','1'),
(26,1,204,'1','1','1','1'),
(27,3,204,'1','1','1','1'),
(28,2,204,'1','1','1','1'),
(29,1,205,'1','1','1','1'),
(30,3,205,'1','1','1','1'),
(31,2,205,'1','1','1','1'),
(32,3,207,'1','1','1','1'),
(33,2,207,'1','1','1','1');

/*Table structure for table `asset_usages` */

DROP TABLE IF EXISTS `asset_usages`;

CREATE TABLE `asset_usages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL,
  `model` varchar(64) DEFAULT NULL,
  `foreign_key` varchar(36) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `url` varchar(512) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `params` text,
  PRIMARY KEY (`id`),
  KEY `fk_asset_usage` (`model`,`foreign_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `asset_usages` */

/*Table structure for table `assets` */

DROP TABLE IF EXISTS `assets`;

CREATE TABLE `assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_asset_id` int(11) DEFAULT NULL,
  `foreign_key` varchar(36) DEFAULT NULL,
  `model` varchar(64) DEFAULT NULL,
  `filename` varchar(255) NOT NULL,
  `filesize` int(16) DEFAULT NULL,
  `width` int(16) DEFAULT NULL,
  `height` int(16) DEFAULT NULL,
  `mime_type` varchar(32) NOT NULL,
  `extension` varchar(5) DEFAULT NULL,
  `hash` varchar(64) DEFAULT NULL,
  `path` varchar(255) NOT NULL,
  `adapter` varchar(32) DEFAULT NULL COMMENT 'Gaufrette Storage Adapter Class',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `un_assets_dimension` (`parent_asset_id`,`width`,`height`),
  KEY `ix_assets_hash` (`hash`,`path`),
  KEY `fk_assets` (`model`,`foreign_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `assets` */

/*Table structure for table `attachments` */

DROP TABLE IF EXISTS `attachments`;

CREATE TABLE `attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `sticky` tinyint(1) NOT NULL DEFAULT '0',
  `visibility_roles` text COLLATE utf8_unicode_ci,
  `hash` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plugin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `import_path` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `asset_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_attachments_hash` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `attachments` */

/*Table structure for table `blocks` */

DROP TABLE IF EXISTS `blocks`;

CREATE TABLE `blocks` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `region_id` int(20) DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `show_title` tinyint(1) NOT NULL DEFAULT '1',
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `element` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visibility_roles` text COLLATE utf8_unicode_ci,
  `visibility_paths` text COLLATE utf8_unicode_ci,
  `visibility_php` text COLLATE utf8_unicode_ci,
  `params` text COLLATE utf8_unicode_ci,
  `publish_start` datetime DEFAULT NULL,
  `publish_end` datetime DEFAULT NULL,
  `updated` datetime NOT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `created` datetime NOT NULL,
  `created_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `block_alias` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `blocks` */

insert  into `blocks`(`id`,`region_id`,`title`,`alias`,`body`,`show_title`,`class`,`status`,`weight`,`element`,`visibility_roles`,`visibility_paths`,`visibility_php`,`params`,`publish_start`,`publish_end`,`updated`,`updated_by`,`created`,`created_by`) values 
(3,4,'About','about','This is the content of your block. Can be modified in admin panel.',1,'',0,2,'','','','','',NULL,NULL,'2018-11-28 05:48:30',1,'2009-07-26 17:13:14',NULL),
(5,4,'Meta','meta','[menu:meta]',1,'',0,6,'','','','','',NULL,NULL,'2018-11-28 05:48:43',1,'2009-09-12 06:36:22',NULL),
(6,4,'Blogroll','blogroll','[menu:blogroll]',1,'',0,4,'','','','','',NULL,NULL,'2018-11-28 05:48:37',1,'2009-09-12 23:33:27',NULL),
(7,4,'Categories','categories','[vocabulary:categories type=\"blog\"]',1,'post-category-widget',1,3,'','','','','',NULL,NULL,'2018-11-28 08:47:15',1,'2009-10-03 16:52:50',NULL),
(8,4,'Search','search','',0,'',1,1,'Nodes.search','','','','',NULL,NULL,'2018-11-28 08:47:13',1,'2009-12-20 03:07:27',NULL),
(9,4,'Recent Posts','recent_posts','[node:recent_posts conditions=\"Node.type:blog\" order=\"Node.id DESC\" limit=\"5\"]',1,'popular-post-widget',1,5,'','','','','',NULL,NULL,'2018-11-28 08:45:11',1,'2009-12-22 05:17:32',NULL);

/*Table structure for table `comments` */

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `parent_id` int(20) DEFAULT NULL,
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Node',
  `foreign_key` int(20) NOT NULL,
  `user_id` int(20) NOT NULL DEFAULT '0',
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `rating` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `comment_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'comment',
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `updated` datetime NOT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `created` datetime NOT NULL,
  `created_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_fk` (`model`,`foreign_key`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `comments` */

insert  into `comments`(`id`,`parent_id`,`model`,`foreign_key`,`user_id`,`name`,`email`,`website`,`ip`,`title`,`body`,`rating`,`status`,`notify`,`type`,`comment_type`,`lft`,`rght`,`updated`,`updated_by`,`created`,`created_by`) values 
(1,NULL,'Node',1,0,'Mr Croogo','email@example.com','http://www.croogo.org','127.0.0.1','','Hi, this is the first comment.',NULL,1,0,'blog','comment',1,2,'2009-12-25 12:00:00',NULL,'2009-12-25 12:00:00',NULL),
(2,NULL,'Node',1,0,'muchamad ichsan','muhammad.iksan3107@gmail.com','','::1',NULL,'test commnet',NULL,1,0,'blog','comment',3,4,'2018-11-26 05:43:39',NULL,'2018-11-26 05:43:39',NULL);

/*Table structure for table `contacts` */

DROP TABLE IF EXISTS `contacts`;

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `address2` text COLLATE utf8_unicode_ci,
  `state` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postcode` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message_status` tinyint(1) NOT NULL DEFAULT '1',
  `message_archive` tinyint(1) NOT NULL DEFAULT '1',
  `message_count` int(11) NOT NULL DEFAULT '0',
  `message_spam_protection` tinyint(1) NOT NULL DEFAULT '0',
  `message_captcha` tinyint(1) NOT NULL DEFAULT '0',
  `message_notify` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `updated` datetime NOT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `created` datetime NOT NULL,
  `created_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `contacts` */

insert  into `contacts`(`id`,`title`,`alias`,`body`,`name`,`position`,`address`,`address2`,`state`,`country`,`postcode`,`phone`,`fax`,`email`,`message_status`,`message_archive`,`message_count`,`message_spam_protection`,`message_captcha`,`message_notify`,`status`,`updated`,`updated_by`,`created`,`created_by`) values 
(1,'Contact','contact','','','','','','','','','','','oktavitri.ervina@gmail.com',1,0,0,0,0,1,1,'2019-05-12 03:40:13',1,'2009-09-16 01:45:17',NULL);

/*Table structure for table `dashboards` */

DROP TABLE IF EXISTS `dashboards`;

CREATE TABLE `dashboards` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_id` int(20) NOT NULL DEFAULT '0',
  `column` int(20) NOT NULL DEFAULT '0',
  `weight` int(20) NOT NULL DEFAULT '0',
  `collapsed` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `dashboards` */

/*Table structure for table `i18n` */

DROP TABLE IF EXISTS `i18n`;

CREATE TABLE `i18n` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `locale` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `locale` (`locale`),
  KEY `model` (`model`),
  KEY `row_id` (`foreign_key`),
  KEY `field` (`field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `i18n` */

/*Table structure for table `languages` */

DROP TABLE IF EXISTS `languages`;

CREATE TABLE `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `native` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `weight` int(11) DEFAULT NULL,
  `updated` datetime NOT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `created` datetime NOT NULL,
  `created_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `languages` */

insert  into `languages`(`id`,`title`,`native`,`alias`,`status`,`weight`,`updated`,`updated_by`,`created`,`created_by`) values 
(1,'English','English','eng',1,1,'2009-11-02 21:37:38',NULL,'2009-11-02 20:52:00',NULL);

/*Table structure for table `links` */

DROP TABLE IF EXISTS `links`;

CREATE TABLE `links` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `parent_id` int(20) DEFAULT NULL,
  `menu_id` int(20) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `visibility_roles` text COLLATE utf8_unicode_ci,
  `params` text COLLATE utf8_unicode_ci,
  `publish_start` datetime DEFAULT NULL,
  `publish_end` datetime DEFAULT NULL,
  `updated` datetime NOT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `created` datetime NOT NULL,
  `created_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `links` */

insert  into `links`(`id`,`parent_id`,`menu_id`,`title`,`class`,`description`,`link`,`target`,`rel`,`status`,`lft`,`rght`,`visibility_roles`,`params`,`publish_start`,`publish_end`,`updated`,`updated_by`,`created`,`created_by`) values 
(5,NULL,4,'About','about','','plugin:nodes/controller:nodes/action:view/type:page/slug:about','','',1,3,4,'','',NULL,NULL,'2009-10-06 23:14:21',NULL,'2009-08-19 12:23:33',NULL),
(6,NULL,4,'Contact','contact','','plugin:contacts/controller:contacts/action:view/contact','','',1,5,6,'','',NULL,NULL,'2009-10-06 23:14:45',NULL,'2009-08-19 12:34:56',NULL),
(7,NULL,3,'Home','home','','plugin:nodes/controller:nodes/action:promoted','','',1,5,6,'','',NULL,NULL,'2009-10-06 21:17:06',NULL,'2009-09-06 21:32:54',NULL),
(8,NULL,3,'About','about','','plugin:nodes/controller:nodes/action:view/type:page/slug:about','','',1,7,10,'','',NULL,NULL,'2009-09-12 03:45:53',NULL,'2009-09-06 21:34:57',NULL),
(9,8,3,'Child link','child-link','','#','','',0,8,9,'','',NULL,NULL,'2009-10-06 23:13:06',NULL,'2009-09-12 03:52:23',NULL),
(10,NULL,5,'Site Admin','site-admin','','/admin','','',1,1,2,'','',NULL,NULL,'2009-09-12 06:34:09',NULL,'2009-09-12 06:34:09',NULL),
(11,NULL,5,'Log out','log-out','','/plugin:users/controller:users/action:logout','','',1,7,8,'[\"1\",\"2\"]','',NULL,NULL,'2009-09-12 06:35:22',NULL,'2009-09-12 06:34:41',NULL),
(12,NULL,6,'Croogo','croogo','','http://www.croogo.org','','',1,3,4,'','',NULL,NULL,'2009-09-12 23:31:59',NULL,'2009-09-12 23:31:59',NULL),
(14,NULL,6,'CakePHP','cakephp','','http://www.cakephp.org','','',1,1,2,'','',NULL,NULL,'2009-10-07 03:25:25',NULL,'2009-09-12 23:38:43',NULL),
(15,NULL,3,'Contact','contact','','/plugin:contacts/controller:contacts/action:view/contact','','',1,29,30,'','',NULL,NULL,'2009-09-16 07:54:13',NULL,'2009-09-16 07:53:33',NULL),
(16,NULL,5,'Entries (RSS)','entries-rss','','/promoted.rss','','',1,3,4,'','',NULL,NULL,'2009-10-27 17:46:22',NULL,'2009-10-27 17:46:22',NULL),
(17,NULL,5,'Comments (RSS)','comments-rss','','/comments.rss','','',1,5,6,'','',NULL,NULL,'2009-10-27 17:46:54',NULL,'2009-10-27 17:46:54',NULL),
(19,NULL,3,'Blog','blog','','plugin:nodes/controller:nodes/action:index/type:blog','','',0,27,28,'','',NULL,NULL,'2019-05-11 08:11:02',1,'2018-12-05 05:52:05',1),
(20,NULL,3,'Berita','berita','','plugin:nodes/controller:nodes/action:index/type:news','','',1,23,24,'','',NULL,NULL,'2018-12-05 06:10:30',1,'2018-12-05 05:57:53',1),
(21,NULL,3,'Testimonial','testimonial','','plugin:nodes/controller:nodes/action:index/type:testimonial','','',1,25,26,'','',NULL,NULL,'2018-12-07 01:07:40',1,'2018-12-06 08:30:49',1),
(22,NULL,3,'Kursus','kursus','','javascript:void(0)','','',1,11,22,'','',NULL,NULL,'2019-05-12 02:08:37',1,'2019-05-12 02:08:37',1),
(23,22,3,'Level A1','level-a1','','plugin:nodes/controller:nodes/action:view/type:page/slug:level-a1','','',1,12,13,'','',NULL,NULL,'2019-05-12 02:12:38',1,'2019-05-12 02:12:38',1),
(24,22,3,'Level A2','level-a2','','plugin:nodes/controller:nodes/action:view/type:page/slug:level-a2','','',1,14,15,'','',NULL,NULL,'2019-05-12 02:25:50',1,'2019-05-12 02:25:50',1),
(25,22,3,'Level B1','level-b1','','plugin:nodes/controller:nodes/action:view/type:page/slug:level-b1','','',1,16,17,'','',NULL,NULL,'2019-05-12 02:26:19',1,'2019-05-12 02:26:19',1),
(26,22,3,'Level B2','level-b2','','plugin:nodes/controller:nodes/action:view/type:page/slug:level-b2','','',1,18,19,'','',NULL,NULL,'2019-05-12 02:26:39',1,'2019-05-12 02:26:39',1),
(27,22,3,'Level C1','level-c1','','plugin:nodes/controller:nodes/action:view/type:page/slug:level-c1','','',1,20,21,'','',NULL,NULL,'2019-05-12 02:33:30',1,'2019-05-12 02:33:30',1),
(28,NULL,7,'Level A1','level-a1','','plugin:nodes/controller:nodes/action:view/type:page/slug:level-a1','','',1,1,2,'','',NULL,NULL,'2019-05-12 03:44:05',1,'2019-05-12 03:44:05',1),
(29,NULL,7,'Level A2','level-a2','','plugin:nodes/controller:nodes/action:view/type:page/slug:level-a2','','',1,3,4,'','',NULL,NULL,'2019-05-12 03:44:17',1,'2019-05-12 03:44:17',1),
(30,NULL,7,'Level B1','level-b1','','plugin:nodes/controller:nodes/action:view/type:page/slug:level-b1','','',1,5,6,'','',NULL,NULL,'2019-05-12 03:44:33',1,'2019-05-12 03:44:33',1),
(31,NULL,7,'Level B2','level-b2','','plugin:nodes/controller:nodes/action:view/type:page/slug:level-b2','','',1,7,8,'','',NULL,NULL,'2019-05-12 03:44:46',1,'2019-05-12 03:44:46',1),
(32,NULL,7,'Level C1','level-c1','','plugin:nodes/controller:nodes/action:view/type:page/slug:level-c1','','',1,9,10,'','',NULL,NULL,'2019-05-12 03:44:59',1,'2019-05-12 03:44:59',1);

/*Table structure for table `menus` */

DROP TABLE IF EXISTS `menus`;

CREATE TABLE `menus` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` int(1) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `link_count` int(11) NOT NULL,
  `params` text COLLATE utf8_unicode_ci,
  `publish_start` datetime DEFAULT NULL,
  `publish_end` datetime DEFAULT NULL,
  `updated` datetime NOT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `created` datetime NOT NULL,
  `created_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menu_alias` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `menus` */

insert  into `menus`(`id`,`title`,`alias`,`class`,`description`,`status`,`weight`,`link_count`,`params`,`publish_start`,`publish_end`,`updated`,`updated_by`,`created`,`created_by`) values 
(3,'Main Menu','main','','',1,NULL,13,'',NULL,NULL,'2009-08-19 12:21:06',NULL,'2009-07-22 01:49:53',NULL),
(4,'Footer','footer','','',1,NULL,2,'',NULL,NULL,'2009-08-19 12:22:42',NULL,'2009-08-19 12:22:42',NULL),
(5,'Meta','meta','','',0,NULL,4,'',NULL,NULL,'2018-12-07 01:39:33',1,'2009-09-12 06:33:29',NULL),
(6,'Blogroll','blogroll','','',0,NULL,2,'',NULL,NULL,'2018-12-07 01:39:32',1,'2009-09-12 23:30:24',NULL),
(7,'Kursus','kursus','','',1,NULL,5,'',NULL,NULL,'2019-05-12 03:43:39',1,'2019-05-12 03:43:39',1);

/*Table structure for table `messages` */

DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `message_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `updated` datetime NOT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `created` datetime NOT NULL,
  `created_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `messages` */

/*Table structure for table `meta` */

DROP TABLE IF EXISTS `meta`;

CREATE TABLE `meta` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Node',
  `foreign_key` int(20) DEFAULT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `weight` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `meta` */

insert  into `meta`(`id`,`model`,`foreign_key`,`key`,`value`,`weight`,`created`,`created_by`,`updated`,`updated_by`) values 
(1,'Node',1,'meta_keywords','key1, key2',NULL,'2018-11-23 01:27:30',NULL,'2018-11-26 06:31:08',1);

/*Table structure for table `model_taxonomies` */

DROP TABLE IF EXISTS `model_taxonomies`;

CREATE TABLE `model_taxonomies` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Node',
  `foreign_key` int(20) NOT NULL DEFAULT '0',
  `taxonomy_id` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `model_taxonomies` */

/*Table structure for table `nodeattachments` */

DROP TABLE IF EXISTS `nodeattachments`;

CREATE TABLE `nodeattachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node_id` int(11) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mime_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `licence` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `priority` int(11) DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `nodeattachments` */

insert  into `nodeattachments`(`id`,`node_id`,`slug`,`path`,`mime_type`,`type`,`title`,`description`,`licence`,`author`,`author_url`,`priority`,`status`,`updated`,`created`) values 
(27,7,'people_2562626_960_720_b5c7a1c36fa5a68b374164dbc709f5a0_f86828c10fd93435cf34d102bca813a8_600x400.jpg','/uploads/people_2562626_960_720_b5c7a1c36fa5a68b374164dbc709f5a0_f86828c10fd93435cf34d102bca813a8_600x400.jpg','image/jpeg','Main','people_2562626_960_720_b5c7a1c36fa5a68b374164dbc709f5a0_f86828c10fd93435cf34d102bca813a8_600x400.jpg','',NULL,'','',1,1,'2019-05-12 04:29:04','2019-05-12 04:24:30'),
(22,1,'Capture4273.PNG','/uploads/Capture4273.PNG','image/png',NULL,'Capture4273.PNG',NULL,NULL,NULL,NULL,1,1,'2018-11-26 06:35:01','2018-11-26 06:35:01'),
(23,1,'lean67.jpg','/uploads/lean67.jpg','image/jpeg',NULL,'lean67.jpg',NULL,NULL,NULL,NULL,1,1,'2018-11-26 06:35:14','2018-11-26 06:35:14'),
(24,1,'LogoSample_ByTailorBrands_267.jpg','/uploads/LogoSample_ByTailorBrands_267.jpg','image/jpeg',NULL,'LogoSample_ByTailorBrands_267.jpg',NULL,NULL,NULL,NULL,1,1,'2018-11-26 06:39:32','2018-11-26 06:39:32'),
(28,8,'45569646_30315.jpg','/uploads/45569646_30315.jpg','image/jpeg','Main','45569646_30315.jpg','',NULL,'','',1,1,'2019-05-12 04:30:51','2019-05-12 04:30:43'),
(29,6,'16429421_30382.jpg','/uploads/16429421_30382.jpg','image/jpeg','Main','16429421_30382.jpg','',NULL,'','',1,1,'2019-05-12 04:35:56','2019-05-12 04:35:51'),
(30,9,'45569662_30342.jpg','/uploads/45569662_30342.jpg','image/jpeg','Main','45569662_30342.jpg','',NULL,'','',1,1,'2019-05-12 04:37:16','2019-05-12 04:37:10');

/*Table structure for table `nodes` */

DROP TABLE IF EXISTS `nodes`;

CREATE TABLE `nodes` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `parent_id` int(20) DEFAULT NULL,
  `user_id` int(20) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8_unicode_ci,
  `status` int(1) DEFAULT NULL,
  `mime_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment_status` int(1) NOT NULL DEFAULT '1',
  `comment_count` int(11) DEFAULT '0',
  `promote` tinyint(1) NOT NULL DEFAULT '0',
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `terms` text COLLATE utf8_unicode_ci,
  `sticky` tinyint(1) NOT NULL DEFAULT '0',
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `visibility_roles` text COLLATE utf8_unicode_ci,
  `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'node',
  `publish_start` datetime DEFAULT NULL,
  `publish_end` datetime DEFAULT NULL,
  `updated` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `nodes` */

insert  into `nodes`(`id`,`parent_id`,`user_id`,`title`,`slug`,`body`,`excerpt`,`status`,`mime_type`,`comment_status`,`comment_count`,`promote`,`path`,`terms`,`sticky`,`lft`,`rght`,`visibility_roles`,`type`,`publish_start`,`publish_end`,`updated`,`updated_by`,`created`,`created_by`) values 
(1,NULL,1,'Hello World','hello-world','<p>Welcome to Croogo. This is your first post. You can edit or delete it from the admin panel.</p>\r\n','',1,'',2,2,0,'/blog/hello-world','{\"1\":\"uncategorized\"}',0,1,2,'','blog',NULL,NULL,'2018-11-26 06:31:08',1,'2009-12-25 11:00:00',NULL),
(2,NULL,1,'About','about','<p>This is an example of a Croogo page, you could edit this to put information about yourself or your site.</p>\r\n','',1,'',0,0,0,'/about','',0,1,2,'','page',NULL,NULL,'2018-11-26 09:02:28',1,'2009-12-25 22:00:00',NULL),
(5,NULL,1,'Sabina','sabina','Frau, saya sudah selesai ujian, alhamdulillah semuanya lancar, yang kita bahas semuanya sangat membantu Frau, terima kasih Frau atas latihan-latihannya, gambaran ujiannya sesuai juga, sekali lagi terima kasih','',1,NULL,1,0,0,'/testimonial/sabina','',0,3,4,'','testimonial',NULL,NULL,'2018-12-06 06:36:53',1,'2018-12-06 06:22:00',1),
(6,NULL,1,'Wirtschaftsinformatik, Jurusan yang Banyak Dinanti Perusahaan Jerman','wirtschaftsinformatik-jurusan-yang-banyak-dinanti-perusahaan-jerman','<p>Seperti apa kuliah di jurusan Wirtschaftsinformatik yang menggabungkan disiplin ilmu komputer, manajemen dan ekonomi. Dalam perbincangan dengan mahasiswa Indonesia, Shelly Nurul Farhani, DW hadirkan ceritanya.</p>\r\n\r\n<p>Jurusan&nbsp;Wirtschaftsinformatik, atau dalam bahasa Inggris disebut dengan&nbsp;Business Information System, memang jurusan yang belum begitu lama ditawarkan di Jerman, kira-kira baru ada pada dekade lalu.</p>\r\n\r\n<p>Seperti apa kira-kira gambaran umum&nbsp;berkuliah&nbsp;di jurusan ini dan bagaimana prospek para alumni&nbsp;untuk dapat terserap bekerja di perusahaan-perusahaan di Jerman.&nbsp;DW Kampus kali ini menghadirkan percakapan dengan dara kelahiran Sukabumi, Jawa Barat, tahun 1995 ini.</p>\r\n\r\n<p>Hallo Shelly, coba ceritakan secara singkat mengenai jurusanmu dan apa saja yang dipelajari?</p>\r\n\r\n<p>Jadi ini&nbsp;adalah jurusan gabungan antara informatika dan bisnis. Di sini kita belajar juga&nbsp;coding, pembukuan, dan ekonomi. Dan jurusan ini adanya belum lama karena itu lulusannya juga belum banyak, jadi kalau lulus dari sini&nbsp;biasanya perusahaan yang&nbsp;nyari&nbsp;kita bukan kita yang&nbsp;nyari&nbsp;perusahaan. Di jurusan ini bidang kerjanya lebih luas karena kita bisa pilih mau lebih mendalami jurusan komputer atau informatika atau lebih mendalami ke bidang bisnis. Ketika semester lima ada penjurusannya kita juga bisa pilih mau lebih mendalami bidang apa.</p>\r\n\r\n<p>Bagaimana awalnya bisa tahu mengenai&nbsp;jurusan tersebut?<br />\r\nAwalnya aku dapat informasi mengenai jurusan ini dari guru aku di&nbsp;Studienkolleg. Dulu itu karena aku masuk di W-Kurs atau kursus tentang ekonomi. Nah, salah satu guru waktu itu bilang kalau dari jurusan itu kita bisa daftar&nbsp;Wirtschaftsinformatik&nbsp;itu gabungan antara informatika&nbsp;dan ekonomi. Setelah aku&nbsp;baca-baca dan pelajari tentang jurusannya dan prospek kerjanya, barulah aku daftar. Kalau di sini aku kuliah di Hochschule Bonn-Rhein-Sieg. Karena ini adalah Hochschule dan bukan universitas, jadi di sini lebih banyak ilmu terapan dibandingkan ilmu teori.</p>\r\n\r\n<p>Suka dukanya kuliah jurusan di jurusan Wirtschaftsinformatik?<br />\r\nSukanya karena untuk prospek kerjanya di Jerman masih banyak&nbsp;banget&nbsp;dicari orang-orang lulusan&nbsp;Wirtschaftsinformatik. Jadi bisa dibilang setelah lulus itu bukan kita yang mencari pekerjaan tetapi pekerjaan yang mencari kita. Gaji lulusannya juga bagus. Kalau dukanya ya itu kuliahnya susah, kita harus benar-benar tekun, tidak bisa berleha-leha.</p>\r\n\r\n<p>Bagaimana interaksi antara dosen dan mahasiswa di jurusan ini?<br />\r\nKuliah di sini harus mahasiswanya yang aktif. Kalau mahasiswa butuh dia yang harus datang ke dosen. Kita yang harus aktif bertanya, bikin janji dan semacamnya. Karena dosen di sini berpikir ya sudah itu (kuliah) urusan kamu. Untuk masalah ujian juga terserah mahasiswa apakah mau ikut ujian atau tidak, jadi lebih bebas.</p>\r\n\r\n<p>Apa tugasnya banyak?<br />\r\nTugas kuliahnya banyak sekali. Mulai dari buat&nbsp;coding, buat makalah sampai presentasi. Tugas yang paling sulit itu buatku ya&nbsp;coding, soalnya&nbsp;&#39;kan&nbsp;banyak dan butuh waktu lama. Terus kita juga harus memastikan tidak ada yang salah. Jadi harus benar-benar benar hahaha. Karena kalau salah nama atau salah koma tidak akan bisa bekerja&nbsp;coding-nya, lalu kalau diunggah juga jadinya gagal. Harus ulang lagi. Itu yang paling susah. Misalnya dalam satu minggu kita harus selesaikan tiga&nbsp;coding, kalau sudah merasa familiar dengan tugasnya bisa selesai dalam waktu 45 menit. Tapi kalau sama sekali belum familiar kita harus tanya orang,&nbsp;cari-cari juga di Google. Ini&nbsp;&#39;nih&nbsp;yang bisa habiskan waktu tiga jam untuk satu&nbsp;coding&nbsp;bahkan bisa seharian.</p>\r\n\r\n<p>Dari mana mendapatkan buku dan materi pembelajaran lainnya?<br />\r\nKalau di sini, buku-buku kita tidak usah beli. Tinggal pinjam saja semua di perpustakaan kampus, sudah ada dan lengkap semua. Materi kuliah juga dari dosen lengkap karena dikirimi langsung lewat email kampus (setiap mahasiswa memiliki email kampus). Jadi sama sekali kuliah selama ini tidak pernah beli buku karena di sini semua ada, lengkap dan gratis.</p>\r\n\r\n<p>Apa yang harus dipersiapkan oleh calon mahasiswa sebelum mengambil kuliah Wirtschaftsinformatik?<br />\r\nJadi buat teman-teman yang tertarik masuk jurusan ini, walaupun di jurusanku semuanya diajari dari dasar sekali tapi dosen akan mengajar dengan sangat cepat. Maka&nbsp;lebih baik sebelum mulai awal perkuliahan belajar dasar-dasar&nbsp;coding&nbsp;dulu yang benar. Selain itu matematikanya juga harus dilancari lagi karena di sini lebih susah. Kalau untuk yang kuliah ekonominya masih bisa &#39;lah aku ikuti. Lalu, kuliahnya juga dalam bahasa Jerman, jadi harus benar-benar dipersiapkan dan dilancarkan lagi karena di sini belum ada kuliah bahasa Inggris buat jurusan&nbsp;Wirtschaftsinformatik.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>source:&nbsp;<a href=\"https://www.dw.com/id/wirtschaftsinformatik-jurusan-yang-banyak-dinanti-perusahaan-jerman/a-45487154\">www.dw.com</a></p>\r\n','',1,NULL,2,0,1,'/nodes/nodes/view/type:news/slug:wirtschaftsinformatik-jurusan-yang-banyak-dinanti-perusahaan-jerman','',0,5,6,'','news',NULL,NULL,'2019-05-12 05:05:04',1,'2019-05-12 01:11:09',1),
(7,NULL,1,'6 Alasan Jerman Jadi Negara Tujuan Studi Calon Mahasiswa Asal Indonesia','6-alasan-jerman-jadi-negara-tujuan-studi-calon-mahasiswa-asal-indonesia','<p>Saat ini banyak calon mahasiswa asal indonesia memilih benua&nbsp;Eropa sebagai tujuannya, seperti Inggris, Perancis, dan Jerman. Dari ketiga negara tersebut, sebagian besar calon mahasiswa Indonesia memilih Jerman sebagai tujuan favoritnya.</p>\r\n\r\n<p>Salah satu seleb ganteng Indonesia yakni Christian Sugiono adalah juga lulusan salah satu universitas di Jerman. Suami Titi Kamal ini merupakan&nbsp;lulusan dari Hamburg University of Technology. Kenapa Jerman sangat digemari oleh para calon mahasiswa Indonesia?</p>\r\n\r\n<h3>1. Universitas di Jerman berkualitas baik</h3>\r\n\r\n<p>Di Jerman ada ratusan universitas, dan semuanya terakreditasi baik. Semua universitas di Jerman sangat bermutu, dan berkualitas secara merata. Banyak sekali universitas Jerman yang masuk kedalam 100 besar universitas terbaik di dunia.</p>\r\n\r\n<p>Di Jerman aspek praktikal juga sangat diperhatikan, jadi bukan hanya teori saja. Pemerintah Jerman sangat mendukung sektor pendidikan, sehingga rata-rata universitas di Jerman memiliki fasilitas laboratorium praktek yang canggih dan berbagai fasilitas modern lainnya untuk mendukung para mahasiswa melakukan berbagai proyek penelitian.</p>\r\n\r\n<p>Jika kamu ingin kuliah di bidang sains dan teknik, maka Jerman adalah pilihan yang tepat. Karena di bidang sains dan teknik, Jerman memang diakui terbaik di dunia.</p>\r\n\r\n<h3>2. Biaya kuliahnya gratis</h3>\r\n\r\n<p>Di Jerman biaya kuliah di subsidi oleh pemerintah. Hal ini tidak hanya berlaku untuk warga negara Jerman saja, tapi juga berlaku untuk seluruh mahasiswa internasional.</p>\r\n\r\n<p>Sehingga jika kamu ingin melanjutkan kuliah di negara ini, kamu tidak perlu pusing memikirkan biaya kuliah dan buku-buku pelajaran. Kamu hanya perlu menyediakan biaya untuk tinggal dan hidup sehari-hari di Jerman.</p>\r\n\r\n<h3>3. Biaya hidup yang relatif terjangkau</h3>\r\n\r\n<p>Bila dibandingkan dengan negara-negara lainnya di Eropa, biaya hidup di Jerman jauh lebih murah dan terjangkau. Bahkan ada yang bilang, biaya hidup di Jerman tidak berbeda jauh dengan Jakarta.</p>\r\n\r\n<p>Di Jerman para mahasiswa biasanya banyak mendapatkan potongan harga (diskon). Contohnya jika ingin menonton film, melihat pertunjukkan musik, dan tempat hiburan lainnya. Kamu hanya perlu menunjukkan kartu mahasiswamu untuk mendapatkan diskon.</p>\r\n\r\n<h3>4. Mendapatkan kesempatan bekerja setelah lulus kuliah</h3>\r\n\r\n<p>Universitas-universitas di Jerman menjalin hubungan yang erat dengan perusahaan-perusahaan sehingga mahasiswanya sangat terbantu sekali mencari lowongan untuk magang kerja selama kuliah, atau untuk bekerja setelah lulus kuliah.</p>\r\n\r\n<p>Pemerintah Jerman memberikan kesempatan kepada para mahasiswa international yang sudah lulus kuliah untuk bekerja di Jerman selama 1,5 tahun. Kebijakan pemerintah Jerman ini tentu sangat membantu para mahasiswa yang baru lulus.</p>\r\n\r\n<h3>5. Jerman termasuk negara Eropa yang aman</h3>\r\n\r\n<p>Keamanan adalah salah satu faktor penting sebelum kamu memutuskan untuk tinggal di suatu tempat. Jerman adalah salah satu negara yang paling aman di Eropa. Selain itu kualitas kehidupan di Jerman sangat baik, hingga negara ini termasuk negara yang rendah angka kriminalitasnya.</p>\r\n\r\n<h3>6. Lulusan universitas Jerman diakui dunia Internasional</h3>\r\n\r\n<p>Di Jerman karena biaya kuliah gratis dan ditanggung oleh pemerintah, maka para mahasiswa harus berusaha keras untuk bisa lulus dengan nilai yang memuaskan. Di sini tidak ada istilah sogok menyogok dosen. Karena para dosen di Jerman kehidupannya sangat diperhatikan oleh pemerintah. Dan mereka hidup dengan sejahtera.</p>\r\n\r\n<p>Di Jerman hanya orang yang memenuhi standar&nbsp;kualitas yang ditetapkanlah yang bisa lulus. Jadi para mahasiswa benar-benar harus berjuang keras, karena itulah lulusan universitas di Jerman sangat diakui oleh dunia internasional.</p>\r\n\r\n<p>Itulah 6 alasan mengapa Jerman menjadi tujuan favorit para calon mahasiswa Indonesia. Selain itu terkadang&nbsp;Pemerintah Jerman juga menyediakan beasiswa bagi para calon mahasiswa asal Indonesia yang ingin study S3 di sana.</p>\r\n\r\n<p>Beasiswa yang disediakan biasanya adalah beasiswa penuh, yang artinya selain bebas biaya kuliah, para mahasiswa juga mendapatkan jaminan tempat tinggal, dan uang saku serta tiket pesawat PP. Untuk informasi beasiswa yang disediakan pemerintah Jerman, kamu bisa mencari informasinya di kedutaan besar Jerman di Indonesia.</p>\r\n\r\n<p>Nah gimana? Udah mulai tertarik untuk kuliah di Jerman? Kalo kamu tertarik, kamu bisa mengumpulkan informasinya mulai dari sekarang.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>source:&nbsp;<a href=\"https://www.idntimes.com/life/education/maria-liana/6-alasan-jerman-jadi-negara-tujuan-studi-calon-mahasiswa-asal-indonesia-c1c2\">www.idntimes.com</a></p>\r\n','',1,NULL,2,0,1,'/nodes/nodes/view/type:news/slug:6-alasan-jerman-jadi-negara-tujuan-studi-calon-mahasiswa-asal-indonesia','',0,7,8,'','news',NULL,NULL,'2019-05-12 05:07:05',1,'2019-05-12 01:17:18',1),
(8,NULL,1,'Yang Harus diketahui sebelum studi di jerman','yang-harus-diketahui-sebelum-studi-di-jerman','<h2>&quot;Bebas Bayaran&quot; Sifatnya Relatif</h2>\r\n\r\n<p>Universitas Jerman hanya bebas bayaran jika calon mahasiswa yang mendaftar ke universitas negeri juga diterima oleh universitas itu. Selain itu, calon mahasiswa juga bermaksud untuk berkuliah dalam kondisi seperti warga Jerman biasa. Itu berarti: menghadapi tantangan yang sama. Program studi yang lain dari itu, atau di universitas swasta, kualitasnya juga bagus, tetapi tidak bebas biaya dan mahal.</p>\r\n\r\n<h2>Mahasiswa dan Kerja Sampingan</h2>\r\n\r\n<p>Visa mahasiswa membatasi jumlah waktu yang boleh digunakan untuk bekerja. Bagi mahasiswa tanpa paspor Uni Eropa, batasnya 120 hari per tahun. Dalam semester kuliah hanya boleh bekerja 20 jam per minggu. Tetapi biaya hidup di Jerman lebih murah daripada di banyak kota AS dan Inggris. Sebaiknya tidak mencoba kerja gelap. Ada risiko eksploitasi, dan jika tertangkap bisa dideportasi.</p>\r\n\r\n<h2>Melamar Beasiswa</h2>\r\n\r\n<p>Di Jerman banyak ditawarkan beasiswa bagi mahasiswa asing di berbagai bidang. Jika berprestasi baik dan ulet mencari beasiswa, kesempatan bisa diperoleh. DAAD adalah lembaga negara Jerman yang memberikan beasiswa paling banyak bagi mahasiswa asing. Yayasan yang memberi beasiswa dengan spesifikasi tertentu juga banyak.</p>\r\n\r\n<h2>Masalah Visa</h2>\r\n\r\n<p>Mahasiswa dari negara bukan anggota Uni Eropa kerap hadapi masalah visa. Tiap orang bertanggungjawab sendiri untuk mengurus asuransi kesehatan, buktik emampuan menunjang hidup secara finansial, temukan tempat tinggal, daftarkan diri pada kantor wilayah, buat janji soal perpanjangan visa, dan dokumen lainnya. Bagi banyak negara, masalah ini sudah dimulai saat meminta visa di kedutaan besar Jerman.</p>\r\n\r\n<h2>Menanggulangi Banyak Formulir</h2>\r\n\r\n<p>Orang harus bersedia mengisi formulir. Sebaiknya biasakan diri dengan kata-kata birokratis Jerman. Juga organisir semua surat, lengkap dengan fotokopinya, mulai dari urusan visa sampai bayar sewa kamar. Triknya: jika dapat surat resmi, kirim kembali surat resmi yang lebih banyak lagi. Begitu saran Leah Scott-Zechlin, yang pernah kuliah di Berlin, dan veteran &quot;Papierkrieg&quot; (perang kertas).</p>\r\n\r\n<h2>Bisa Bahasa Jerman Sangat Membantu</h2>\r\n\r\n<p>Tentu di kota besar orang asing bisa tinggal tanpa bisa bahasa Jerman. Sebagian program studi juga ditawarkan dalam bahasa Inggris. Tetapi setiap aspek hidup lebih mudah jika bisa bahasa Jerman, baik untuk bicara dengan petugas negara, maupun untuk bersosialisasi dengan orang Jerman. Kalau ingin bekerja, kemampuan berbahasa Jerman jadi aset sangat besar di pasaran tenaga kerja.</p>\r\n\r\n<h2>Universitas Tidak Menuntun Mahasiswa</h2>\r\n\r\n<p>Di Jerman mahasiswa tidak dibimbing seperti di sekolah. Sepenuhnya tergantung tiap mahasiswa asing untuk bisa jalani hidup di negara asing, datang ke kuliah dan belajar. Mata kuliah ada yang berkesan sangat bebas. Terserah mahasiswa, apakah serahkan pekerjaan rumah, berpartisipasi dalam kuliah atau tidak. Sebagian mata kuliah tergantung sepenuhnya pada ujian akhir atau makalah di akhir semester.</p>\r\n\r\n<h2>Masalah Tempat Tinggal</h2>\r\n\r\n<p>Asrama mahasiswa ada di banyak kota. Tetapi untuk dapat tempat kadang sulit. Di samping asrama, mahasiswa Jerman juga sering tinggal di Wohngemeinschaft (WG). Dalam sistem ini, beberapa mahasiswa bersama-sama menyewa sebuah apartemen. Tiap orang dapat satu kamar. Dapur dan kamar mandi biasanya digunakan bersama. Ini cara baik untuk bersosialisasi dengan orang Jerman dan memperbaiki bahasa Jerman.</p>\r\n\r\n<h2>Mencari Saran</h2>\r\n\r\n<p>Tinggal dan belajar di luar negeri kerap butuh tanggung jawab tinggi. Dan kadang orang merasa harus berjuang sendirian menghadapi banyak tantangan. Tapi tidak usah khawatir. Anda bukan mahasiswa asing pertama di Jerman. Sumber informasi dan saran kerap bisa ditemukan di internet. Untuk yang berbahasa Inggris ada forum &quot;Toytown Germany&quot;.</p>\r\n\r\n<h2>Mungkin Ingin Tinggal Selamanya</h2>\r\n\r\n<p>Mungkin Anda individu yang tahu cara peroleh kesempatan terbaik dalam hidup: kuliah beberapa tahun di Jerman, raih gelar, mungkin kerja sedikit, lalu kembali ke tanah air dan dapat penghasilan tinggi. Bisa jadi juga, Anda jatuh cinta dengan Jerman, sehingga hadapi dilema ucapkan &quot;Tsch&uuml;&szlig;&quot; (selamat tinggal) selamanya kepada tanah air, atau rindu Jerman seumur hidup.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>source:&nbsp;<a href=\"https://www.dw.com/id/wirtschaftsinformatik-jurusan-yang-banyak-dinanti-perusahaan-jerman/a-45487154\">www.dw.com</a></p>\r\n','',1,NULL,2,0,1,'/nodes/nodes/view/type:news/slug:yang-harus-diketahui-sebelum-studi-di-jerman','',0,9,10,'','news',NULL,NULL,'2019-05-12 05:04:51',1,'2019-05-12 01:26:52',1),
(9,NULL,1,'Kiat Pilih Jurusan Kuliah di Jerman Sesuai Minat dan Kemampuan','kiat-pilih-jurusan-kuliah-di-jerman-sesuai-minat-dan-kemampuan','<h2>Banyak bertanya dan konsultasi</h2>\r\n\r\n<p>Shelly Farhani, mahasiswi jurusan Wirtschaftsinformatik atau Business Information System di Hochscule Bonn-Rhein-Sieg, Sankt Augustin, Jerman, menyarankan para calon mahasiswa untuk tidak ragu bertanya tentang jurusan yang akan dipilih. &quot;Terutama aku dulu tanya ke orang yang sedang kuliah di jurusan itu, atau konsultasi ke orang yang tahu seperti konsultan waktu di Studienkolleg.&quot;</p>\r\n\r\n<h2>Pelajari kurikulumnya</h2>\r\n\r\n<p>Banyak program studi di Jerman yang memberikan informasi terkait kurikulum mereka. Shelly mengatakan lebih baik &quot;pelajari dulu kurikulumnya. Jadi ketika semester baru dimulai kita sudah tahu subjek apa saja yang mesti dipelajari dan didalami lebih jauh.&quot; Dengan mempelajari kurikulum, mahasiswa jadi bisa mengukur kemampuan mereka sesuai tuntutan program studi.</p>\r\n\r\n<h2>Cari tahu prospek kerja para alumni</h2>\r\n\r\n<p>Para calon mahasiswa juga diharapkan untuk tahu prospek pekerjaan bagi para alumni dan seberapa besar kesempatan mereka bekerja. Selain itu, mereka juga harus tahu tingkat persaingan di dunia kerja, khususnya bila harus bersaing dengan orang-orang yang merupakan penutur asli bahasa di negara tersebut. &quot;Penting juga tahu kira-kira besaran gaji yang akan diterima ketika lulus nanti,&quot; ujar Shelly.</p>\r\n\r\n<h2>Tidak bisa berleha-leha</h2>\r\n\r\n<p>Seperti di Indonesia, dunia perkuliahan akan berbeda sekali dengan dunia sekolah. Namun di Jerman mahasiswa diberi lebih banyak lagi kebebasan, termasuk bisa pilih apakah sudah siap ujian atau belum. Bila merasa belum siap, mahasiswa bisa menunda jadwal ujian mereka. Tapi ini bukan berarti mahasiswa bisa menunda dan berleha-leha, karena tugas kuliah padat menanti.</p>\r\n\r\n<h2>Jangan lupa bersosialisasi</h2>\r\n\r\n<p>Tidak diragukan lagi, masa kuliah apalagi S1 di Jerman memang termasuk lama. Selain sibuk dengan urusan perkuliahan, masa ini juga adalah kesempatan bagus untuk bersosialisasi dan membangun relasi dengan kawan dan rekan sekampus. &quot;Memang bisa selesai kuliah 3,5 tahun, tapi itu kalau tidak punya kehidupan sosial,&quot; ujar Shelly.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>source:&nbsp;<a href=\"https://www.dw.com/id/wirtschaftsinformatik-jurusan-yang-banyak-dinanti-perusahaan-jerman/a-45487154\">www.dw.com</a></p>\r\n','',1,NULL,2,0,1,'/nodes/nodes/view/type:news/slug:kiat-pilih-jurusan-kuliah-di-jerman-sesuai-minat-dan-kemampuan','',0,11,12,'','news',NULL,NULL,'2019-05-12 05:03:53',1,'2019-05-12 01:30:47',1),
(10,NULL,1,'Level A1','level-a1','<h1>LEVEL A1</h1>\r\n\r\n<p>ujian Bahasa Jerman untuk dewasa. Kelulusannya mengakui kemampuan Bahasa Jerman yang sangat sederhana. Di dalam Kesepakatan Uni Eropa untuk Jenjang Kompetensi Bahasa yang membagi bahasa dalam skala enam jenjang, ujian ini ada pada jenjang pertama (A1).</p>\r\n\r\n<p>APABILA ANDA LULUS UJIAN INI, ANDA BISA MENUNJUKKAN, BAHWA ANDA &hellip;</p>\r\n\r\n<ul>\r\n	<li>dapat berkomunikasi dengan cara sederhana, jika lawan bicara berbicara lambat dan jelas.</li>\r\n	<li>dapat mengerti dan menggunakan ungkapan-ungkapan sehari-hari dan sering digunakan dan kalimat-kalimat sederhana (misalnya: informasi tentang seseorang dan keluarga, berbelanja, pekerjaan, lingkungan sekitar).</li>\r\n	<li>dapat memperkenalkan diri sendiri dan orang lain, mengajukan pertanyaan kepada orang lain mengenai keseharian orang tersebut &ndash; misalnya di mana mereka tingal, siapa yang mereka kenal, atau benda-benda apa yang mereka punyai.</li>\r\n</ul>\r\n','',1,NULL,1,0,0,'/page/level-a1','',0,13,14,'','page',NULL,NULL,'2019-05-12 02:13:39',1,'2019-05-12 02:11:28',1),
(11,NULL,1,'Level A2','level-a2','<h1>LEVEL A2</h1>\r\n\r\n<p>ujian Bahasa Jerman untuk dewasa. Ujian ini mensyaratkan kemampuan bahasa di jenjang kompetensi kedua (A2) dari Kesepakatan Uni Eropa untuk Jenjang Kompetensi Bahasa yang membagi bahasa dalam skala enam jenjang.</p>\r\n\r\n<p>APABILA KAMU LULUS UJIAN INI, KAMU BISA MENUNJUKKAN, BAHWA KAMU &hellip;</p>\r\n\r\n<ul>\r\n	<li>dapat memahami dan mengerti kalimat-kalimat dan ungkapan-ungkapan yang sering digunakan dalam kehidupan sehari-hari.</li>\r\n	<li>dapat berkomunikasi dalam situasi yang sederhana dan yang menjadi rutinitas sehari-hari, ditandai dengan adanya kemampuan bertukar informasi mengenai hal-hal yang lazim dijumpai sehari-hari.</li>\r\n	<li>dapat mengungkapkan dengan kalimat sederhana asal usul dan pendidikan, lingkungan terdekat dan hal-hal yang berhubungan dengan kebutuhan primer.</li>\r\n</ul>\r\n','',1,NULL,1,0,0,'/page/level-a2','',0,15,16,'','page',NULL,NULL,'2019-05-12 02:25:14',1,'2019-05-12 02:15:30',1),
(12,NULL,1,'Level B1','level-b1','<h1>LEVEL B1</h1>\r\n\r\n<p>ujian Bahasa Jerman untuk remaja dan dewasa. Kelulusannya mengakui penggunaan Bahasa Jerman secara mandiri. Di dalam Kesepakatan Uni Eropa untuk Jenjang Kompetensi Bahasa yang membagi bahasa dalam skala enam jenjang, ujian ini ada pada jenjang ketiga (B1).</p>\r\n\r\n<p>APABILA KAMU LULUS UJIAN INI, KAMU BISA MENUNJUKKAN, BAHWA KAMU &hellip;</p>\r\n\r\n<ul>\r\n	<li>dapat memahami topik utama dari pokok bahasan tertentu yang lazim dibicarakan seperti: pekerjaan, pendidikan, waktu senggang dll, apabila dalam percakapan tersebut digunakan bahasa Jerman standar yang mudah dimengerti.</li>\r\n	<li>dapat mengatasi hampir semua situasi berkenaan dengan kemampuan berbahasa ketika berada di negara-negara berbahasa Jerman.</li>\r\n	<li>dapat menjabarkan dengan mudah dan kontekstual pokok-pokok bahasan tertentu yang lazim dibicarakan dan pokok-pokok bahasan yang Anda sukai yang berkaitan dengan diri Anda sendiri.</li>\r\n	<li>dapat menceritakan pengalaman-pengalaman dan kejadian-kejadian, dapat menjabarkan impian, harapan dan tujuan hidup, juga dapat memberikan alasan atau penjelasan singkat.</li>\r\n</ul>\r\n','',1,NULL,1,0,0,'/page/level-b1','',0,17,18,'','page',NULL,NULL,'2019-05-12 02:22:30',1,'2019-05-12 02:21:22',1),
(13,NULL,1,'Level B2','level-b2','<h1>LEVEL B2</h1>\r\n\r\n<p>ujian bahasa Jerman untuk orang dewasa&nbsp;dan mulai 2019 juga untuk remaja. Ujian ini mengonfirmasi kecakapan bahasa pada tingkat lanjut dan setara dengan jenjang keempat (B2) pada skala kompetensi Kesepakatan Uni Eropa untuk Jenjang Kompetensi Bahasa yang memiliki enam jenjang.</p>\r\n\r\n<p>APABILA ANDA LULUS UJIAN INI, ANDA BISA MENUNJUKKAN, BAHWA ANDA &hellip;</p>\r\n\r\n<ul>\r\n	<li>dapat memahami intisari dari teks rumit mengenai tema konkrit dan abstrak, dapat memahami diskusi ilmiah dengan pokok bahasan yang sesuai dengan bidang yang dikuasainya.</li>\r\n	<li>dapat berkomunikasi dengan Penutur Bahasa Ibu dengan spontan dan lancar sehingga tercipta percakapan yang wajar dan tanpa hambatan besar pada kedua belah pihak.</li>\r\n	<li>dapat menyampaikan pendapat dengan jelas dan rinci tentang berbagai tema, mempertahankan pendapat tentang sebuah persoalan yang aktual, dapat menyampaikan sisi baik dan sisi buruk dari berbagai kemungkinan.</li>\r\n</ul>\r\n','',1,NULL,1,0,0,'/page/level-b2','',0,19,20,'','page',NULL,NULL,'2019-05-12 02:24:17',1,'2019-05-12 02:24:17',1),
(14,NULL,1,'Level C1','level-c1','<h1>LEVEL C1</h1>\r\n\r\n<p>ujian Bahasa Jerman untuk dewasa. Kelulusannya mengakui penggunaan Bahasa Jerman tingkat lanjutan atas. Di dalam Kesepakatan Uni Eropa untuk Jenjang Kompetensi Bahasa yang membagi bahasa dalam skala enam jenjang, ujian ini ada pada jenjang kelima (C1).</p>\r\n\r\n<p>APABILA ANDA LULUS UJIAN INI, ANDA BISA MENUNJUKKAN, BAHWA ANDA &hellip;</p>\r\n\r\n<ul>\r\n	<li>dapat memahami beragam teks yang rumit dan relatif panjang serta dapat menangkap makna implisit.</li>\r\n	<li>dapat mengemukakan pendapat dengan spontan dan lancar tanpa sering terlihat kesulitan dalam mencari kata yang tepat.</li>\r\n	<li>dapat menggunakan Bahasa Jerman dengan tepat dan fleksibel dalam bidang kehidupan sosial dan pekerjaan atau dalam bidang pendidikan.</li>\r\n	<li>dapat menerangkan pokok bahasan yang rumit dengan jelas, terstruktur dan rinci.</li>\r\n</ul>\r\n','',1,NULL,1,0,0,'/page/level-c1','',0,21,22,'','page',NULL,NULL,'2019-05-12 02:33:00',1,'2019-05-12 02:33:00',1),
(15,NULL,1,'Muhammad Agung','muhammad-agung','Buat lu semua yang pengen kuliah ke jerman tapi ga tau caranya, belajar privat aja sama frau Vina. Nanti lu bisa tanya2 tuh ke dia step2 berangkat ke jerman + semua ttg jerman. Ketimbang lu pake agen mahel, mending sama frau vin aja ( Kursus A1 - B2 )','',1,NULL,1,0,0,'/testimonial/muhammad-agung','',0,23,24,'','testimonial',NULL,NULL,'2019-05-12 03:17:45',1,'2019-05-12 03:17:00',1),
(16,NULL,1,'Fazil Aulia','fazil-aulia','Recommended nih sama kak Ervina, dulu 2013 kurz sebelum los ke jerman buat ANP sempet diprivatin sama kak Ervin. :D','',1,NULL,1,0,0,'/testimonial/fazil-aulia','',0,25,26,'','testimonial',NULL,NULL,'2019-05-12 03:19:17',1,'2019-05-12 03:19:17',1),
(17,NULL,1,'Yona','yona','Setelah Yona UN bln mei, kita lanjut ya kak lesnya, Di weekdays aja kalo bisa (Kursus A1)','',1,NULL,1,0,0,'/testimonial/yona','',0,27,28,'','testimonial',NULL,NULL,'2019-05-12 03:21:05',1,'2019-05-12 03:20:48',1),
(18,NULL,1,'Yuli Setiawan','yuli-setiawan','Assalamualaikum, slamat pagi Frau Vina :) Saya mau ucapin trimakasih atas bimbingan dari kesabarannya selama saya belajar Deutch A2 kemarin. Alhamdulillah saya dapet beasiswa DAAD nya mbak. insyaAllah kalo lancar akhir maret brangkat ke jerman. Terimakasih ya mbak vina atas ilmunya (Kursus A2)','',1,NULL,1,0,0,'/testimonial/yuli-setiawan','',0,29,30,'','testimonial',NULL,NULL,'2019-05-12 03:23:37',1,'2019-05-12 03:23:37',1);

/*Table structure for table `regions` */

DROP TABLE IF EXISTS `regions`;

CREATE TABLE `regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `block_count` int(11) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `region_alias` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `regions` */

insert  into `regions`(`id`,`title`,`alias`,`description`,`block_count`,`created`,`created_by`,`updated`,`updated_by`) values 
(3,'none','none','',0,'2018-11-23 01:27:30',NULL,'2018-11-23 01:27:30',NULL),
(4,'right','right','',3,'2018-11-23 01:27:30',NULL,'2018-11-23 01:27:30',NULL),
(6,'left','left','',0,'2018-11-23 01:27:30',NULL,'2018-11-23 01:27:30',NULL),
(7,'header','header','',0,'2018-11-23 01:27:30',NULL,'2018-11-23 01:27:30',NULL),
(8,'footer','footer','',0,'2018-11-23 01:27:31',NULL,'2018-11-23 01:27:31',NULL),
(9,'region1','region1','',0,'2018-11-23 01:27:31',NULL,'2018-11-23 01:27:31',NULL),
(10,'region2','region2','',0,'2018-11-23 01:27:31',NULL,'2018-11-23 01:27:31',NULL),
(11,'region3','region3','',0,'2018-11-23 01:27:31',NULL,'2018-11-23 01:27:31',NULL),
(12,'region4','region4','',0,'2018-11-23 01:27:31',NULL,'2018-11-23 01:27:31',NULL),
(13,'region5','region5','',0,'2018-11-23 01:27:31',NULL,'2018-11-23 01:27:31',NULL),
(14,'region6','region6','',0,'2018-11-23 01:27:31',NULL,'2018-11-23 01:27:31',NULL),
(15,'region7','region7','',0,'2018-11-23 01:27:31',NULL,'2018-11-23 01:27:31',NULL),
(16,'region8','region8','',0,'2018-11-23 01:27:31',NULL,'2018-11-23 01:27:31',NULL),
(17,'region9','region9','',0,'2018-11-23 01:27:31',NULL,'2018-11-23 01:27:31',NULL),
(18,'share_widgets','share_widgets','A block to contain the social media share links',0,'2018-11-23 01:44:38',NULL,'2018-11-23 01:44:38',NULL);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_alias` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`title`,`alias`,`created`,`created_by`,`updated`,`updated_by`) values 
(1,'Admin','admin','2009-04-05 00:10:34',NULL,'2009-04-05 00:10:34',NULL),
(2,'Registered','registered','2009-04-05 00:10:50',NULL,'2009-04-06 05:20:38',NULL),
(3,'Public','public','2009-04-05 00:12:38',NULL,'2009-04-07 01:41:45',NULL);

/*Table structure for table `roles_users` */

DROP TABLE IF EXISTS `roles_users`;

CREATE TABLE `roles_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `granted_by` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pk_role_users` (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `roles_users` */

/*Table structure for table `schema_migrations` */

DROP TABLE IF EXISTS `schema_migrations`;

CREATE TABLE `schema_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

/*Data for the table `schema_migrations` */

insert  into `schema_migrations`(`id`,`class`,`type`,`created`) values 
(1,'InitMigrations','Migrations','2018-11-23 01:27:24'),
(2,'ConvertVersionToClassNames','Migrations','2018-11-23 01:27:24'),
(3,'IncreaseClassNameLength','Migrations','2018-11-23 01:27:25'),
(4,'FirstMigrationSettings','Settings','2018-11-23 01:27:25'),
(5,'SettingsTrackableFields','Settings','2018-11-23 01:27:25'),
(6,'AddedAssetTimestampSetting','Settings','2018-11-23 01:27:25'),
(7,'ExposeSiteThemeAndLocaleAndHomeUrl','Settings','2018-11-23 01:27:25'),
(8,'FirstMigrationAcl','Acl','2018-11-23 01:27:25'),
(9,'SplitSession','Acl','2018-11-23 01:27:25'),
(10,'FirstMigrationBlocks','Blocks','2018-11-23 01:27:25'),
(11,'BlocksTrackableFields','Blocks','2018-11-23 01:27:26'),
(12,'BlocksPublishingFields','Blocks','2018-11-23 01:27:26'),
(13,'FirstMigrationComments','Comments','2018-11-23 01:27:26'),
(14,'CommentsTrackableFields','Comments','2018-11-23 01:27:26'),
(15,'AddCommentsForeignKeys','Comments','2018-11-23 01:27:26'),
(16,'FirstMigrationContacts','Contacts','2018-11-23 01:27:26'),
(17,'ContactsTrackableFields','Contacts','2018-11-23 01:27:26'),
(18,'FirstMigrationMenus','Menus','2018-11-23 01:27:26'),
(19,'MenusTrackableFields','Menus','2018-11-23 01:27:27'),
(20,'MenusPublishingFields','Menus','2018-11-23 01:27:27'),
(21,'FirstMigrationMeta','Meta','2018-11-23 01:27:27'),
(22,'MetaTrackableFields','Meta','2018-11-23 01:27:27'),
(23,'FirstMigrationNodes','Nodes','2018-11-23 01:27:27'),
(24,'NodesTrackableFields','Nodes','2018-11-23 01:27:27'),
(25,'NodesPublishingFields','Nodes','2018-11-23 01:27:28'),
(26,'FirstMigrationTaxonomy','Taxonomy','2018-11-23 01:27:28'),
(27,'TaxonomyTrackableFields','Taxonomy','2018-11-23 01:27:28'),
(28,'RenameNodesTaxonomy','Taxonomy','2018-11-23 01:27:28'),
(29,'AddModelTaxonomyForeignKey','Taxonomy','2018-11-23 01:27:28'),
(30,'AddWysisygEnableField','Taxonomy','2018-11-23 01:27:28'),
(31,'FirstMigrationUsers','Users','2018-11-23 01:27:29'),
(32,'UsersTrackableFields','Users','2018-11-23 01:27:29'),
(33,'UsersEnlargeTimezone','Users','2018-11-23 01:27:29'),
(34,'FirstMigrationDashboard','Dashboards','2018-11-23 01:27:29'),
(35,'FirstMigrationTranslate','Translate','2018-11-23 01:28:56'),
(36,'TranslateTrackableFields','Translate','2018-11-23 01:28:57'),
(37,'FirstMigrationAssets','Assets','2018-11-26 02:50:35'),
(38,'CounterCache','Assets','2018-11-26 02:50:35');

/*Table structure for table `seos` */

DROP TABLE IF EXISTS `seos`;

CREATE TABLE `seos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `node_id` int(10) DEFAULT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `meta_robots` text COLLATE utf8_unicode_ci,
  `changefreq` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `priority` tinytext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `seos` */

insert  into `seos`(`id`,`node_id`,`meta_keywords`,`meta_description`,`meta_robots`,`changefreq`,`priority`) values 
(1,2,'test','ini deskripsi','','',''),
(2,1,'','','','',''),
(3,3,'','','','',''),
(4,4,'','','','',''),
(5,5,'','','','',''),
(6,6,'','','','',''),
(7,7,'','','','',''),
(8,8,'','','','',''),
(9,9,'','','','',''),
(10,10,'','','','',''),
(11,11,'','','','',''),
(12,12,'','','','',''),
(13,13,'','','','',''),
(14,14,'','','','',''),
(15,15,'','','','',''),
(16,16,'','','','',''),
(17,17,'','','','',''),
(18,18,'','','','','');

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `input_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `editable` tinyint(1) NOT NULL DEFAULT '1',
  `weight` int(11) DEFAULT NULL,
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `settings` */

insert  into `settings`(`id`,`key`,`value`,`title`,`description`,`input_type`,`editable`,`weight`,`params`,`created`,`created_by`,`updated`,`updated_by`) values 
(6,'Site.title','BISA JERMAN','','','',1,1,'','2018-11-23 01:27:31',NULL,'2019-05-12 04:21:46',1),
(7,'Site.tagline','Terbaik untuk belajar bahasa jerman dengan baik dan cepat','','','textarea',1,2,'','2018-11-23 01:27:31',NULL,'2019-05-12 04:21:46',1),
(8,'Site.email','oktavitri.ervina@gmail.com ','','','',1,3,'','2018-11-23 01:27:31',NULL,'2019-05-12 04:21:46',1),
(9,'Site.status','1','','','checkbox',1,6,'','2018-11-23 01:27:31',NULL,'2019-05-12 04:21:46',1),
(12,'Meta.robots','index, follow','','','',1,6,'','2018-11-23 01:27:31',NULL,'2018-11-26 06:27:13',1),
(13,'Meta.keywords','croogo, Croogo','','','textarea',1,7,'','2018-11-23 01:27:31',NULL,'2018-11-26 06:27:13',1),
(14,'Meta.description','Bisa Jerman adalah sebuah lembaga pendidikan bahasa jerman','','','textarea',1,8,'','2018-11-23 01:27:31',NULL,'2018-11-26 06:27:13',1),
(15,'Meta.generator','Croogo - Content Management System','','','',0,9,'','2018-11-23 01:27:31',NULL,'2018-11-23 01:27:31',NULL),
(16,'Service.akismet_key','3f1619c2b04e','','','',1,11,'','2018-11-23 01:27:31',NULL,'2018-11-26 06:15:58',1),
(17,'Service.recaptcha_public_key','your-public-key','','','',1,12,'','2018-11-23 01:27:31',NULL,'2018-11-26 06:15:58',1),
(18,'Service.recaptcha_private_key','your-private-key','','','',1,13,'','2018-11-23 01:27:31',NULL,'2018-11-26 06:15:58',1),
(19,'Service.akismet_url','http://your-blog.com','','','',1,10,'','2018-11-23 01:27:31',NULL,'2018-11-26 06:15:57',1),
(20,'Site.theme','Flatly','','','',0,14,'','2018-11-23 01:27:31',NULL,'2018-11-23 01:27:31',1),
(21,'Site.feed_url','','','','',0,15,'','2018-11-23 01:27:31',NULL,'2018-11-23 01:27:31',NULL),
(22,'Reading.nodes_per_page','4','','','',1,16,'','2018-11-23 01:27:31',NULL,'2018-12-05 07:44:29',1),
(23,'Writing.wysiwyg','1','Enable WYSIWYG editor','','checkbox',1,17,'','2018-11-23 01:27:31',NULL,'2018-11-23 01:27:31',NULL),
(24,'Comment.level','2','','levels deep (threaded comments)','',1,18,'','2018-11-23 01:27:31',NULL,'2018-12-06 08:54:50',1),
(25,'Comment.feed_limit','10','','number of comments to show in feed','',1,19,'','2018-11-23 01:27:31',NULL,'2018-12-06 08:54:50',1),
(26,'Site.locale','eng','','','text',1,20,'','2018-11-23 01:27:31',NULL,'2019-05-12 04:21:46',1),
(27,'Reading.date_time_format','D, M d Y H:i:s','','','',1,21,'','2018-11-23 01:27:31',NULL,'2018-12-05 07:44:29',1),
(28,'Comment.date_time_format','M d, Y','','','',1,22,'','2018-11-23 01:27:31',NULL,'2018-12-06 08:54:50',1),
(29,'Site.timezone','UTC','','Provide a valid timezone identifier as specified in https://php.net/manual/en/timezones.php','',1,4,'','2018-11-23 01:27:31',NULL,'2019-05-12 04:21:46',1),
(32,'Hook.bootstraps','Settings,Contacts,Nodes,Meta,Menus,Users,Blocks,Taxonomy,FileManager,Wysiwyg,Ckeditor,Comments,Translate,Seo,Image2,Nodeattachmen,Nodeattachment','','','',0,23,'','2018-11-23 01:27:31',NULL,'2018-11-23 01:27:31',1),
(33,'Comment.email_notification','1','Enable email notification','','checkbox',1,24,'','2018-11-23 01:27:31',NULL,'2018-12-06 08:54:50',1),
(34,'Access Control.multiRole','0','Enable Multiple Roles','','checkbox',1,25,'','2018-11-23 01:27:31',NULL,'2018-11-23 01:27:31',NULL),
(35,'Access Control.rowLevel','0','Row Level Access Control','','checkbox',1,26,'','2018-11-23 01:27:31',NULL,'2018-11-23 01:27:31',NULL),
(36,'Access Control.autoLoginDuration','+1 week','\"Remember Me\" Duration','Eg: +1 day, +1 week. Leave empty to disable.','text',1,27,'','2018-11-23 01:27:31',NULL,'2018-11-23 01:27:31',NULL),
(37,'Access Control.models','','Models with Row Level Acl','Select models to activate Row Level Access Control on','multiple',1,26,'multiple=checkbox\noptions={\"Nodes.Node\": \"Node\", \"Blocks.Block\": \"Block\", \"Menus.Menu\": \"Menu\", \"Menus.Link\": \"Link\"}','2018-11-23 01:27:31',NULL,'2018-11-23 01:27:31',NULL),
(38,'Site.ipWhitelist','127.0.0.1','Whitelisted IP Addresses','Separate multiple IP addresses with comma','text',1,27,'','2018-11-23 01:27:31',NULL,'2019-05-12 04:21:46',1),
(39,'Site.asset_timestamp','force','Asset timestamp','Appends a timestamp which is last modified time of the particular file at the end of asset files URLs (CSS, JavaScript, Image). Useful to prevent visitors to visit the site with an outdated version of these files in their browser cache.','radio',1,28,'options={\"0\": \"Disabled\", \"1\": \"Enabled in debug mode only\", \"force\": \"Always enabled\"}','2018-11-23 01:27:31',NULL,'2019-05-12 04:21:46',1),
(40,'Site.admin_theme','','Administration Theme','','text',1,29,'','2018-11-23 01:27:31',NULL,'2019-05-12 04:21:46',1),
(41,'Site.home_url','','Home Url','Default action for home page in link string format.','text',1,30,'','2018-11-23 01:27:31',NULL,'2019-05-12 04:21:46',1),
(42,'Croogo.installed','1','','','',0,31,'','2018-11-23 01:27:55',NULL,'2018-11-23 01:27:55',NULL),
(43,'Croogo.version','2.3.2','','','',0,32,'','2018-11-23 01:28:37',1,'2018-11-23 01:28:37',1),
(44,'Seo.remove_settings_on_deactivate','NO','','Remove settings on deactivate','',1,33,'','2018-11-23 01:44:37',1,'2018-11-26 06:22:08',1),
(45,'Seo.changefreq','weekly','','Default Changefeq of the SEO Sitemap entries','',1,34,'','2018-11-23 01:44:37',1,'2018-11-26 06:22:08',1),
(46,'Seo.priority','0.8','','Default Priority of the SEO Sitemap entries','',1,35,'','2018-11-23 01:44:37',1,'2018-11-26 06:22:08',1),
(47,'Seo.organize_by_vocabulary','1','','Organize the public sitemap by vocabulary?','',1,36,'','2018-11-23 01:44:37',1,'2018-11-26 06:22:09',1),
(48,'Seo.homepage_title','Bisa Jerman','','Homepage Title ','',1,37,'','2018-11-23 01:44:37',1,'2018-11-26 06:22:09',1),
(49,'Seo.homepage_description','bisa jerman adalah tempat dimana Anda dapat kursur bahasa jerman','','Default Homepage META Description','',1,38,'','2018-11-23 01:44:37',1,'2018-11-26 06:22:09',1),
(50,'Seo.show_per_page_stats','0','','Show Page Stats','',1,39,'','2018-11-23 01:44:37',1,'2018-11-26 06:22:09',1),
(51,'Seo.hook_google','0','','Provide Hook to Google','',1,40,'','2018-11-23 01:44:37',1,'2018-11-26 06:22:09',1),
(52,'Seo.hook_twitter','0','','Provide Hook to Twitter','',1,41,'','2018-11-23 01:44:37',1,'2018-11-26 06:22:09',1),
(53,'Seo.hook_facebook','0','','Provide Hook to Facebook','',1,42,'','2018-11-23 01:44:37',1,'2018-11-23 01:44:37',1),
(54,'Seo.alexa_verification_key','','','Alexa Verification Key','',1,43,'','2018-11-23 01:44:37',1,'2018-11-26 06:22:08',1),
(55,'Seo.bing_webmaster_tools_key','','','Bing Webmaster Tools Key','',1,44,'','2018-11-23 01:44:37',1,'2018-11-26 06:22:08',1),
(56,'Seo.google_adwords_tracking_for_messages','','','Google AdWords Tracking for Messages','',1,45,'','2018-11-23 01:44:37',1,'2018-11-23 01:44:37',1),
(57,'Seo.google_webmaster_tools_key','','','Google Webmaster Tools Key','',1,46,'','2018-11-23 01:44:37',1,'2018-11-23 01:44:37',1),
(58,'Seo.google_analytics_ua','','','Google Analytics UA Property','',1,47,'','2018-11-23 01:44:37',1,'2018-11-23 01:44:37',1),
(59,'Seo.google_analytics_domain','','','Google Analytics Domain','',1,48,'','2018-11-23 01:44:37',1,'2018-11-23 01:44:37',1),
(60,'Seo.google_places_cid','','','Google Places CID','',1,49,'','2018-11-23 01:44:37',1,'2018-11-23 01:44:37',1),
(61,'Seo.google_plus_cid','','','Google Plus CID','',1,50,'','2018-11-23 01:44:37',1,'2018-11-23 01:44:37',1),
(62,'Seo.google_tag_manager','XXXX-XXXX','','Google Tag Manager Code','',1,51,'','2018-11-23 01:44:37',1,'2018-11-23 01:44:37',1),
(63,'Seo.meta_robots_default','INDEX, FOLLOW','','Default robots entry for individual pages','',1,52,'','2018-11-23 01:44:37',1,'2018-11-26 06:22:08',1),
(64,'Seo.insert_meta_description','1','','Insert META Description tag?','',1,53,'','2018-11-23 01:44:37',1,'2018-11-26 06:22:08',1),
(65,'Seo.insert_meta_robots','1','','Insert META Robots tag?','',1,54,'','2018-11-23 01:44:37',1,'2018-11-26 06:22:08',1),
(66,'Seo.insert_meta_keywords','1','','Insert META Keywords tag?','',1,55,'','2018-11-23 01:44:37',1,'2018-11-26 06:22:08',1),
(67,'Seo.turn_off_promote_by_default','1','','Turn OFF \"Promoted\" by default','',1,56,'','2018-11-23 01:44:37',1,'2018-11-23 01:44:37',1),
(68,'Seo.add_rss_ga_campaign_tags','1','','Add Google Analytics Campaign Trackers to link?','',1,57,'','2018-11-23 01:44:37',1,'2018-11-26 06:22:09',1),
(69,'Seo.rss_ga_medium','rssfeed','','Campaign Medium','',1,58,'','2018-11-23 01:44:37',1,'2018-11-26 06:22:09',1),
(70,'Seo.rss_ga_campaign_name','RSSFeed','','Campaign Name','',1,59,'','2018-11-23 01:44:37',1,'2018-11-26 06:22:09',1),
(71,'Seo.rss_before','','','RSS Post Prefix','',1,60,'','2018-11-23 01:44:37',1,'2018-11-26 06:22:09',1),
(72,'Seo.rss_after','','','RSS Post Suffix','',1,61,'','2018-11-23 01:44:37',1,'2018-11-26 06:22:09',1),
(73,'Seo.add_copy_link','1','','Add page link when copied?','',1,62,'','2018-11-23 01:44:38',1,'2018-11-26 06:22:09',1),
(74,'Seo.add_copy_link_ga_campaign_tags','1','','Add Google Analytics Campaign Trackers to link?','',1,63,'','2018-11-23 01:44:38',1,'2018-11-26 06:22:09',1),
(75,'Seo.copy_link_ga_medium','copylink','','Campaign Medium','',1,64,'','2018-11-23 01:44:38',1,'2018-11-26 06:22:09',1),
(76,'Seo.copy_link_ga_campaign_name','CutNPaste','','Campaign Name','',1,65,'','2018-11-23 01:44:38',1,'2018-11-26 06:22:09',1),
(77,'Seo.copy_link_text','Read more at: {{current_page}} Copyright &copy; {{site_title}}','','Text to add when copied.','',1,66,'','2018-11-23 01:44:38',1,'2018-11-26 06:22:09',1),
(78,'Seo.facebook_link','','','Facebook Page','',1,67,'','2018-11-23 01:44:38',1,'2018-11-23 01:44:38',1),
(79,'Seo.facebook_app_key','','','Facebook App Key','',1,68,'','2018-11-23 01:44:38',1,'2018-11-23 01:44:38',1),
(80,'Seo.facebook_app_secret','','','Facebook App Secret','',1,69,'','2018-11-23 01:44:38',1,'2018-11-23 01:44:38',1),
(81,'Seo.twitter_username','','','Twitter Username','',1,70,'','2018-11-23 01:44:38',1,'2018-11-23 01:44:38',1),
(82,'Seo.twitter_consumer_key','','','Twitter Consumer Key','',1,71,'','2018-11-23 01:44:38',1,'2018-11-23 01:44:38',1),
(83,'Seo.twitter_consumer_secret','','','Twitter Consumer Secret','',1,72,'','2018-11-23 01:44:38',1,'2018-11-23 01:44:38',1),
(84,'Seo.twitter_access_token','','','Twitter Access Token','',1,73,'','2018-11-23 01:44:38',1,'2018-11-23 01:44:38',1),
(85,'Seo.twitter_access_token_secret','','','Twitter Access Secret','',1,74,'','2018-11-23 01:44:38',1,'2018-11-23 01:44:38',1),
(86,'Nodeattachment.maxFileSize','2','','Max. size of uploaded file (MB)','',1,75,'','2018-11-23 02:12:58',1,'2018-11-26 09:04:01',1),
(87,'Nodeattachment.allowedFileTypes','jpg,gif,png','','Coma separated list of allowes extensions (empty = all files)','',1,76,'','2018-11-23 02:12:58',1,'2018-11-26 09:04:01',1),
(88,'Nodeattachment.storageUploadDir','','','Full path to directory for big files. You can use it for FTP files uploading','',1,77,'','2018-11-23 02:12:58',1,'2018-11-26 09:04:01',1),
(89,'Nodeattachment.ffmpegDir','n/a','','Directory with ffmpeg, type n/a if not installed','',1,78,'','2018-11-23 02:12:58',1,'2018-11-26 09:04:01',1),
(90,'Nodeattachment.types','Main,Gallery','','Coma separated list of attachment types','',1,79,'','2018-11-23 02:12:58',1,'2018-11-26 09:04:01',1),
(91,'Nodeattachment.ffmpegExec','0','','Run ffmpeg via exec(), more info about permission at http://en.php.net/manual/en/function.exec.php ','checkbox',1,80,'','2018-11-23 02:12:58',1,'2018-11-26 09:04:01',1),
(92,'Seo.adwords_conversion_key_contact','','','Conversion ID','',1,81,'','2018-11-23 02:14:38',1,'2018-11-23 02:14:38',1),
(93,'Seo.adwords_conversion_language_contact','','','Conversion Language','',1,82,'','2018-11-23 02:14:38',1,'2018-11-23 02:14:38',1),
(94,'Seo.adwords_conversion_format_contact','','','Conversion Format','',1,83,'','2018-11-23 02:14:38',1,'2018-11-23 02:14:38',1),
(95,'Seo.adwords_conversion_color_contact','','','Conversion Color','',1,84,'','2018-11-23 02:14:38',1,'2018-11-23 02:14:38',1),
(96,'Seo.adwords_conversion_label_contact','','','Conversion Label','',1,85,'','2018-11-23 02:14:38',1,'2018-11-23 02:14:38',1),
(97,'Seo.adwords_conversion_value_contact','','','Conversion Value','',1,86,'','2018-11-23 02:14:38',1,'2018-11-23 02:14:38',1),
(98,'Assets.installed','1','','','',0,87,'','2018-11-26 02:50:35',1,'2018-11-26 02:50:35',1),
(99,'SocialMedia.facebook','','',NULL,'text',1,10,'','2019-05-11 14:44:53',1,'2018-12-06 06:12:52',1),
(100,'SocialMedia.twitter','','',NULL,'text',1,20,'','2019-05-11 14:44:58',1,'2018-12-06 06:12:52',1),
(101,'SocialMedia.instagram','https://www.instagram.com/lesprivat.jerman/','',NULL,'text',1,30,'','2019-05-11 14:45:01',1,'2018-12-06 06:12:52',1),
(102,'Site.phone','081386801043','',NULL,'text',1,3,'',NULL,NULL,'2019-05-12 04:21:46',1),
(104,'Site.favicon','/uploads/logo/favicon.ico','',NULL,'file',1,10,'',NULL,NULL,'2019-05-12 04:21:46',1),
(105,'Miscellaneous.experiences','6','',NULL,'text',1,1,'',NULL,NULL,'2018-12-06 03:05:48',1),
(106,'Miscellaneous.students','35','',NULL,'text',1,2,'',NULL,NULL,'2018-12-06 03:05:48',1),
(107,'Miscellaneous.reason','Kami menerapkan pedoman untuk tepat target belajar sesuai kemampuan belajar yang diambil.','','Reason Point','textarea',1,3,'',NULL,NULL,'2018-12-06 03:05:48',1);

/*Table structure for table `taxonomies` */

DROP TABLE IF EXISTS `taxonomies`;

CREATE TABLE `taxonomies` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `parent_id` int(20) DEFAULT NULL,
  `term_id` int(10) NOT NULL,
  `vocabulary_id` int(10) NOT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `taxonomies` */

insert  into `taxonomies`(`id`,`parent_id`,`term_id`,`vocabulary_id`,`lft`,`rght`) values 
(1,NULL,1,1,1,2),
(2,NULL,2,1,3,4),
(3,NULL,3,2,1,2),
(4,NULL,4,2,3,4);

/*Table structure for table `terms` */

DROP TABLE IF EXISTS `terms`;

CREATE TABLE `terms` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `updated` datetime NOT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `created` datetime NOT NULL,
  `created_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `terms` */

insert  into `terms`(`id`,`title`,`slug`,`description`,`updated`,`updated_by`,`created`,`created_by`) values 
(1,'Uncategorized','uncategorized','','2009-07-22 03:38:43',NULL,'2009-07-22 03:34:56',NULL),
(2,'Announcements','announcements','','2010-05-16 23:57:06',NULL,'2009-07-22 03:45:37',NULL),
(3,'mytag','mytag','','2009-08-26 14:42:43',NULL,'2009-08-26 14:42:43',NULL),
(4,'Kursus','kursus','','2018-11-28 04:10:59',1,'2018-11-28 04:10:59',1);

/*Table structure for table `types` */

DROP TABLE IF EXISTS `types`;

CREATE TABLE `types` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `format_show_author` tinyint(1) NOT NULL DEFAULT '1',
  `format_show_date` tinyint(1) NOT NULL DEFAULT '1',
  `format_use_wysiwyg` tinyint(1) NOT NULL DEFAULT '1',
  `comment_status` int(1) NOT NULL DEFAULT '1',
  `comment_approve` tinyint(1) NOT NULL DEFAULT '1',
  `comment_spam_protection` tinyint(1) NOT NULL DEFAULT '0',
  `comment_captcha` tinyint(1) NOT NULL DEFAULT '0',
  `params` text COLLATE utf8_unicode_ci,
  `plugin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated` datetime NOT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `created` datetime NOT NULL,
  `created_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type_alias` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `types` */

insert  into `types`(`id`,`title`,`alias`,`description`,`format_show_author`,`format_show_date`,`format_use_wysiwyg`,`comment_status`,`comment_approve`,`comment_spam_protection`,`comment_captcha`,`params`,`plugin`,`updated`,`updated_by`,`created`,`created_by`) values 
(1,'Page','page','A page is a simple method for creating and displaying information that rarely changes, such as an \"About us\" section of a website. By default, a page entry does not allow visitor comments.',0,0,1,0,1,0,0,'',NULL,'2009-09-09 00:23:24',NULL,'2009-09-02 18:06:27',NULL),
(2,'Blog','blog','A blog entry is a single post to an online journal, or blog.',1,1,1,2,1,0,0,'',NULL,'2009-09-15 12:15:43',NULL,'2009-09-02 18:20:44',NULL),
(4,'Node','node','Default content type.',1,1,1,2,1,0,0,'',NULL,'2009-10-06 21:53:15',NULL,'2009-09-05 23:51:56',NULL),
(5,'News','news','berisi berita-berita yang akan disampaikan',1,1,1,2,0,0,0,'',NULL,'2019-05-11 10:37:53',1,'2018-11-28 07:16:01',1),
(6,'Testimonial','testimonial','ini adalah konten untuk testimonial',0,0,0,0,0,0,0,'',NULL,'2018-12-05 08:14:47',1,'2018-12-05 08:11:14',1);

/*Table structure for table `types_vocabularies` */

DROP TABLE IF EXISTS `types_vocabularies`;

CREATE TABLE `types_vocabularies` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type_id` int(10) NOT NULL,
  `vocabulary_id` int(10) NOT NULL,
  `weight` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `types_vocabularies` */

insert  into `types_vocabularies`(`id`,`type_id`,`vocabulary_id`,`weight`) values 
(24,4,1,NULL),
(25,4,2,NULL),
(30,2,1,NULL),
(31,2,2,NULL),
(33,6,4,NULL),
(35,5,3,NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `username` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activation_key` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `updated` datetime NOT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `created` datetime NOT NULL,
  `timezone` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `created_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`role_id`,`username`,`password`,`name`,`email`,`website`,`activation_key`,`image`,`bio`,`status`,`updated`,`updated_by`,`created`,`timezone`,`created_by`) values 
(1,1,'admin','3ec57f4ff9a56ff3c1dabbb06af22336a7f59cca','admin','',NULL,'eb5655780c7e5b814c441592ddb09a40',NULL,NULL,1,'2019-05-11 09:57:22',NULL,'2018-11-23 01:27:55','0',NULL);

/*Table structure for table `vocabularies` */

DROP TABLE IF EXISTS `vocabularies`;

CREATE TABLE `vocabularies` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `multiple` tinyint(1) NOT NULL DEFAULT '0',
  `tags` tinyint(1) NOT NULL DEFAULT '0',
  `plugin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `updated` datetime NOT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `created` datetime NOT NULL,
  `created_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vocabulary_alias` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `vocabularies` */

insert  into `vocabularies`(`id`,`title`,`alias`,`description`,`required`,`multiple`,`tags`,`plugin`,`weight`,`updated`,`updated_by`,`created`,`created_by`) values 
(1,'Categories','categories','',0,1,0,NULL,1,'2010-05-17 20:03:11',NULL,'2009-07-22 02:16:21',NULL),
(2,'Tags','tags','',0,1,0,NULL,2,'2010-05-17 20:03:11',NULL,'2009-07-22 02:16:34',NULL),
(3,'News','news','',0,0,0,NULL,3,'2018-11-28 07:16:33',1,'2018-11-28 07:16:33',1),
(4,'testimonial','testimonial','',0,0,0,NULL,4,'2018-12-05 08:10:03',1,'2018-12-05 08:10:03',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
