<?php
		$this->Html->addCrumb($contact['Contact']['title'], 'javascript:void(0)');

		$email 	= Configure::read('Site.email');
		$phone 	= Configure::read('Site.phone');
?>
<section class="contact-page-area section-gap no-padd-top">
	<div class="iframe-container">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d991.5740396847903!2d106.89390803839507!3d-6.224626291840349!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f34e0eedd539%3A0xc2e67ada7c657252!2sbisajerman.com!5e0!3m2!1sen!2sid!4v1557646862283!5m2!1sen!2sid" height="275" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
	
	<div class="container">
		<div class="row contact-wrap">
			<div class="col-lg-4 d-flex flex-column address-wrap">
				<?php
						if(!empty($phone)){
				?>
				<div class="single-contact-address d-flex flex-row">
					<div class="icon">
						<span class="lnr lnr-phone-handset"></span>
					</div>
					<div class="contact-details">
						<h5><?php echo $this->Custom->splitString($phone);?></h5>
					</div>
				</div>
				<?php
						}

						if(!empty($email)){
				?>
				<div class="single-contact-address d-flex flex-row">
					<div class="icon">
						<span class="lnr lnr-envelope"></span>
					</div>
					<div class="contact-details">
						<h5><?php echo $email;?></h5>
						<p>Kirimkan kami permintaan Anda</p>
					</div>
				</div>
				<?php
						}
				?>
			</div>
			<?php if ($contact['Contact']['message_status']):  ?>
			<div class="col-lg-8">
				<?php
						echo $this->Form->create('Message', array(
							'url' => array(
								'plugin' => 'contacts',
								'controller' => 'contacts',
								'action' => 'view',
								$contact['Contact']['alias'],
							),
							'class' => 'form-area contact-form text-right',
							'id' => 'myForm'
						));
				?>
				<div class="row">
					<div class="col-lg-6">
						<?php
								echo $this->Form->input('Message.name', array(
									'placeholder' => __('Nama'),
									'class' => 'common-input mb-20 form-control',
									'onfocus' => "this.placeholder = ''",
									'onblur' => "this.placeholder = 'Nama'",
									'label' => false,
									'div' => false
								)); 

								echo $this->Form->input('Message.email', array(
									'type' => 'email',
									'placeholder' => __('Email'),
									'pattern' => "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$",
									'onfocus' => "this.placeholder = ''",
						 			'onblur' => "this.placeholder = 'Email'",
						 			'class' => 'common-input mb-20 form-control',
						 			'label' => false,
						 			'div' => false
								)); 

								echo $this->Form->input('Message.title', array(
									'placeholder' => __('Subjek'),
									'class' => 'common-input mb-20 form-control',
									'onfocus' => "this.placeholder = ''",
									'onblur' => "this.placeholder = 'Subjek'",
									'label' => false,
									'div' => false
								)); 
							?>
					</div>
					<div class="col-lg-6">
						<?php
								echo $this->Form->input('Message.body', array(
									'type' => 'textarea',
									'placeholder' => __('Pesan'),
									'class' => "common-textarea form-control",
									'onfocus' => "this.placeholder = ''",
						 			'onblur' => "this.placeholder = 'Pesan'",
						 			'label' => false,
						 			'div' => false
								)); 

								if ($contact['Contact']['message_captcha']):
									echo $this->Recaptcha->display_form(array(
										'div' => 'mt-20 pull-right'
									));
								endif; 
						?>
					</div>
					<div class="col-lg-12 mt-30">
						<div class="alert-msg" style="text-align: left;"></div>
						<?php
								echo $this->Form->button(__('Kirim'), array(
									'class' => 'primary-btn primary',
									'style' => 'float: right;'
								));
						?>
					</div>
				</div>
				<?php echo $this->Form->end();?>
			</div>
			<?php endif;?>
		</div>
	</div>
</section>
<script>
	$(document).ready(function(){
		$('.iframe-container').click(function(){
			$(this).find('iframe').addClass('clicked')
		}).mouseleave(function(){
			$(this).find('iframe').removeClass('clicked')
		});
	});
</script>