<?php 
		$this->Nodes->set($node); 

		$url 			= $this->Html->url($this->Nodes->field('url'), true);
		$created 		= $this->Nodes->field('created');
		$comment_count 	= $this->Nodes->field('comment_count');
		$excerpt 		= $this->Nodes->field('excerpt');
		$title 			= $this->Nodes->field('title');
		$type 			= $this->Nodes->field('type');
		$comment_status = $this->Nodes->field('comment_status');

		$is_page		= ($type == 'page');

		$name = $node['User']['name'];

		$thumbnail = '';
		if(!empty($node['Nodeattachment'])){
			foreach ($node['Nodeattachment'] as $key => $value) {
				if($value['type'] == 'Main'){
					$image = $value;
				}
			}

			if(!empty($image)){
				$this->Nodeattachment->setNodeattachment($image);

				$thumbnail = $this->Image2->resize($this->Nodeattachment->field('thumb_path'), 720, 420, 'resizeRatio', 
					array(
						'alt' => $this->Nodeattachment->field('slug'),
						'class' => 'img-fluid'
					), 
					false, $this->Nodeattachment->field('server_thumb_path'));
			}
		}

		$class_content = 'col-lg-9 col-md-9';
		$class_content_main = 'col-lg-8';
		if($is_page){
			$class_content = 'col-lg-12 col-md-12';
			$class_content_main = 'col-lg-12';
		}else{
			$this->Html->addCrumb($title, 'javascript:void(0)');
		}
?>
<section class="post-content-area single-post-area">
	<div class="container">
		<div class="row">
			<div class="<?php echo $class_content_main;?> posts-list">
				<div class="single-post row">
					<?php
							if(!empty($thumbnail)){
					?>
					<div class="col-lg-12">
						<div class="feature-img">
							<?php echo $thumbnail;?>
						</div>
					</div>
					<?php
							}

							if(!$is_page){
					?>
					<div class="col-lg-3 col-md-3 meta-details">
						<?php
								echo $this->Custom->tags($node['Taxonomy'], $type);
						?>
						<div class="user-details row">
							<p class="user-name col-lg-12 col-md-12 col-6"><a href="javascript:void(0)"><?php echo $name;?></a> <span class="lnr lnr-user"></span></p>
							<p class="date col-lg-12 col-md-12 col-6"><a href="javascript:void(0)"><?php echo $this->Time->format('d M, Y', $created);?></a> <span class="lnr lnr-calendar-full"></span></p>
							<p class="comments col-lg-12 col-md-12 col-6"><a href="javascript:void(0)"><?php echo $comment_count.' Comments';?></a> <span class="lnr lnr-bubble"></span></p>
							<ul class="social-links col-lg-12 col-md-12 col-6">
								<li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode($url)?>" target="blank"><i class="fa fa-facebook"></i></a></li>
								<li><a href="https://twitter.com/home?status=<?php echo urlencode($title.' - '.$url)?>" target="blank"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
					<?php
							}
					?>
					<div class="<?php echo $class_content;?>">
						<?php
								echo $this->Nodes->body();
						?>
					</div>
					<?php
							if(!empty($excerpt)){
					?>
					<div class="col-lg-12">
						<div class="quotes"><?php echo $excerpt;?></div>
					</div>
					<?php
							}
					?>
				</div>
				<?php 
						if (CakePlugin::loaded('Comments') ): 
							$type = $types_for_layout[$this->Nodes->field('type')];

							if ($type['Type']['comment_status'] > 0 && $comment_status > 0 && !empty($comments)) {
								echo $this->Html->div('comments-area', $this->element('Comments.comments', array('model' => 'Node', 'data' => $node)));
							}

							if ($type['Type']['comment_status'] == 2 && $comment_status == 2) {
								echo $this->element('Comments.comments_form', array('model' => 'Node', 'data' => $node));
							}
						endif; 
				?>
			</div>
			<?php
					if(!$is_page){
			?>
			<div class="col-lg-4 sidebar-widgets">
				<?php echo $this->element('sidebar');?>
			</div>
			<?php
					}
			?>
		</div>
	</div>
</section>