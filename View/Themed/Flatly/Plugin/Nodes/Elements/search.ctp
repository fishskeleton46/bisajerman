<div class="single-sidebar-widget search-widget">
	<?php
			echo $this->Form->create('Node', array(
				'url' => array(
					'admin' => false, 
					'plugin' => 'nodes', 
					'controller' => 'nodes', 
					'action' => 'search', 
					'class' => 'form-search'
				),
				'class' => 'search-form form-search'
			));

			$this->Form->unlockField('q');

			echo $this->Form->input('q', array(
				'label' => false,
				'placeholder' => __('Search'),
				'onfocus' => "this.placeholder = ''",
				'onblur' => "this.placeholder = 'Search'"
			));

			echo $this->Form->button('<i class="fa fa-search"></i>', array(
				'type' => 'submit'
			));

			echo $this->Form->end();
	?>
</div>