<div class="comment-list" id="comment-<?php echo $comment['Comment']['id']; ?>" class="well comment<?php if ($node['Node']['user_id'] == $comment['Comment']['user_id']) { echo ' author'; } ?>">
	<div class="single-comment justify-content-between d-flex">
		<div class="user justify-content-between d-flex">
			<div class="thumb">
				<?php echo $this->Html->image('http://www.gravatar.com/avatar/' . md5(strtolower($comment['Comment']['email'])) . '?s=32', array('class' => 'img-rounded img-polaroid')) ?>
			</div>
			<div class="desc">
				<?php
						if ($comment['Comment']['website'] != null) {
							$link = $this->Html->link($comment['Comment']['name'], $comment['Comment']['website'], array('target' => '_blank'));
						} else {
							$link = $this->Html->link($comment['Comment']['name'], 'javascript:void(0)');
						}

						echo $this->Html->tag('h5', $link);
				?>
				<p class="date"><?php echo __('said on %s', $this->Time->format(Configure::read('Comment.date_time_format'), $comment['Comment']['created'], null, Configure::read('Site.timezone'))); ?></p>
				<p class="comment"><?php echo nl2br($this->Text->autoLink($comment['Comment']['body'])); ?></p>
			</div>
		</div>
		<?php
			if ($level <= Configure::read('Comment.level')) {
				echo $this->Html->div('reply-btn', $this->Html->link(__('Reply'), array(
					'plugin' => 'comments',
					'controller' => 'comments',
					'action' => 'add',
					'Node',
					$node['Node']['id'],
					$comment['Comment']['id'],
				), array(
					'class' => 'btn-reply text-uppercase',
					'icon' => 'comment',
				)));
			}
		?>
	</div>
	<?php
		if (isset($comment['children']) && count($comment['children']) > 0) {
			foreach ($comment['children'] as $childComment) {
				echo $this->element('Comments.comment', array('comment' => $childComment, 'level' => $level + 1));
			}
		}
	?>
</div>