<div class="comment-form">
	<h4>Leave a Comment</h4>
	<?php
			$type = $types_for_layout[$data[$model]['type']];

			$is_login = ($this->Session->check('Auth.User.id') != '');

			if ($this->request->params['controller'] == 'comments') {
				$backLink = $this->Html->link(__d('croogo', 'Go back to original post') . ': ' . $data[$model]['title'], $data[$model]['url']);
				echo $this->Html->tag('p', $backLink, array('class' => 'back'));
			}

			$formUrl = array(
				'plugin' => 'comments',
				'controller' => 'comments',
				'action' => 'add',
				'Node',
				$data[$model]['id'],
			);

			if (isset($parentId) && $parentId != null) {
				$formUrl[] = $parentId;
			}

			echo $this->Form->create('Comment', array('url' => $formUrl));

			$this->Form->inputDefaults(array(
				'label' => false,
				'class' => 'form-control',
				'onfocus' => "this.placeholder = ''",
				'div' => false
			));
	?>
	<div class="form-group form-inline">
		<div class="form-group col-lg-6 col-md-12 name">
			<?php
					echo $this->Form->input('Comment.name', array(
						'placeholder' => __('Enter Name'),
						'onblur' => "this.placeholder = 'Enter Name'",
						'value' => !empty($is_login) ? $this->Session->read('Auth.User.name') : false,
						'readonly' => !empty($is_login) ? true : false,
					));
			?>
		</div>
		<div class="form-group col-lg-6 col-md-12 email">
			<?php
					echo $this->Form->input('Comment.email', array(
						'placeholder' => __('Enter email address'),
						'onblur' => "this.placeholder = 'Enter email address'",
						'value' => !empty($is_login) ? $this->Session->read('Auth.User.email') : false,
						'readonly' => !empty($is_login) ? true : false,
					));
			?>
		</div>
	</div>
	<div class="form-group">
		<?php
				echo $this->Form->input('Comment.website', array(
					'placeholder' => __('Subject'),
					'onblur' => "this.placeholder = 'Subject'",
				));
		?>
	</div>
	<div class="form-group">
		<?php
				echo $this->Form->input('Comment.body', array(
					'type' => 'textarea',
					'class' => 'form-control mb-10',
					'row' => 5,
					'label' => false,
					'onblur' => "this.placeholder = 'Messege'",
					'placeholder' => 'Messege'
				));
		?>
	</div>
	<?php 
			if ($type['Type']['comment_captcha']) {
				echo $this->Recaptcha->display_form(array(
					'div' => 'mb-15'
				));
			}

			echo $this->Form->button(__('Post comment'), array('class' => 'primary-btn text-uppercase'));	
			echo $this->Form->end();
	?>
</div>