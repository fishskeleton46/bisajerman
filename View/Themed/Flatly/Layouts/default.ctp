<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<title><?php echo Configure::read('Site.title'); ?> | <?php echo $title_for_layout; ?></title>
	<?php
			$meta = $this->Seo->meta();
			
			if(empty($meta)){
				echo $this->Layout->meta();
			}else{
				echo $meta;
			}

			echo $this->Html->css(array(
				'linearicons',
				'font-awesome.min',
				'bootstrap',
				'magnific-popup',
				'nice-select',
				'animate.min',
				'owl.carousel',
			));

			$favicon = !empty(Configure::read('Site.favicon')) ? Configure::read('Site.favicon') : false;

			echo $this->fetch('css');

			echo $this->Html->css(array(
				'main',
			));
	?>
	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">

	<?php
			if(!empty($favicon)){
	?>
	<link rel="shortcut icon" href="<?php echo $favicon;?>" type="image/x-icon">
    <link rel="icon" href="<?php echo $favicon;?>" type="image/x-icon">
	<?php
			}
	?>
</head>

<body>
	<?php 
			echo $this->element('header');

			echo $this->Layout->sessionFlash();

			if ($this->request->url == '') {
	            echo $this->element('homepage');
	        } else {
	        	echo $this->element('breadcrumb');

	            echo $this->fetch('content'); 
	        }

			echo $this->element('footer');

			echo $this->Layout->js();
			echo $this->Html->script(array(
				'vendor/jquery.min',
				'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js',
				'vendor/bootstrap.min',
				'https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA',
				'easing.min',
				'hoverIntent',
				'superfish.min',
				'jquery.ajaxchimp.min',
				'jquery.magnific-popup.min',
				'owl.carousel.min',
				'isotope.pkgd.min',
				'jquery.nice-select.min',
				'jquery.lightbox',
				'mail-script',
			));

			echo $this->fetch('script');

			echo $this->Html->script(array(
				'main',
			));
	?>
</body>

</html>