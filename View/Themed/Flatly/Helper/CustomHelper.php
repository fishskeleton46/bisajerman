<?php
/**
 * Custom Helper
 *
 *
 * @category Helper
 * @package  Croogo
 * @version  1.0
 * @author   Nitish Dhar (@nitishdhar)
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link	 http://www.croogo.org
 */
class CustomHelper extends Helper {

/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
	public $helpers = array(
		'Html' => array(
			'className' => 'Croogo.CroogoHtml',
		),
		'Form' => array(
			'className' => 'Croogo.CroogoHtml',
		),
		'Session',
		'Js',
		'Layout',
	);

	public function menu($menuAlias, $options = array()) {
		$_options = array(
			'tag' => 'ul',
			'tagAttributes' => array(),
			'selected' => 'active',
			'dropdown' => false,
			'dropdownClass' => 'dropdown',
			'dropdownMenuClass' => 'dropdown-menu',
			'toggle' => 'dropdown-toggle',
			'menuClass' => 'nav navbar-nav',
			'element' => 'menu',
		);
		$options = Hash::merge($_options, $options);

		if (!isset($this->_View->viewVars['menus_for_layout'][$menuAlias])) {
			return false;
		}
		$menu = $this->_View->viewVars['menus_for_layout'][$menuAlias];

		$menu['threaded'] = $this->_setActiveMenu($menu['threaded']);

		$output = $this->_View->element($options['element'], array(
			'menu' => $menu,
			'options' => $options,
				));
		return $output;
	}

	protected function _setActiveMenu($menu){
        if(!empty($menu)){
            foreach ($menu as $key => $value) {
                $ref =& $menu[$key];

                if(!empty($value['children'])){
                    $check = $this->_getActiveMenuChildren($value['children']);

                    if(!empty($check)){
                        $ref['active_menu'] = true;
                    }
                }
            }
        }

        return $menu;
    }

    protected function _getActiveMenuChildren($menu){
        if (!empty($this->request->params['locale'])) {
            $currentUrl = substr($this->_View->request->url, strlen($this->request->params['locale']));
        } else {
            $currentUrl = $this->_View->request->url;
        }

        foreach ($menu as $key => $link) {
            $expl = explode('/', $link['Link']['link']);

            $temp = array();
            foreach ($expl as $key => $value) {
                $expl = explode(':', $value);

                $temp[$expl[0]] = $expl[1];
            }

            if (Router::url($temp) == Router::url('/' . $currentUrl)) {
                return true;
                break;
            }else{
                if(!empty($link['children'])){
                    $this->_getActiveMenuChildren($link['children']);
                }
            }
        }


    }

/**
 * Nested Links
 *
 * @param array $links model output (threaded)
 * @param array $options (optional)
 * @param integer $depth depth level
 * @return string
 */
	public function nestedLinks($links, $options = array(), $depth = 1) {
		$_options = array('menuClass' => 'nav-dropdown');
		$options = array_merge($_options, $options);

		$output = '';
		foreach ($links AS $link) {
			$active_menu = !empty($link['active_menu']) ? true : false;
			
			$linkAttr = array(
				'id' => 'link-' . $link['Link']['id'],
				'rel' => $link['Link']['rel'],
				'target' => $link['Link']['target'],
				'title' => $link['Link']['description'],
				'class' => $link['Link']['class'],
			);

			foreach ($linkAttr AS $attrKey => $attrValue) {
				if ($attrValue == null) {
					unset($linkAttr[$attrKey]);
				}
			}

			// if link is in the format: controller:contacts/action:view
			if (strstr($link['Link']['link'], 'controller:')) {
				$link['Link']['link'] = $this->Layout->linkStringToArray($link['Link']['link']);
			}

			// Remove locale part before comparing links
			if (!empty($this->params['locale'])) {
				$currentUrl = substr($this->_View->request->url, strlen($this->params['locale']));
			} else {
				$currentUrl = $this->_View->request->url;
			}

			if ((Router::url($link['Link']['link']) == Router::url('/' . $currentUrl)) || !empty($active_menu)) {
				if (!isset($linkAttr['class'])) {
					$linkAttr['class'] = '';
				}
				$linkAttr['class'] .= ' ' . $options['selected'];
			}

			$linkOutput = $this->Html->link($link['Link']['title'], $link['Link']['link']);
			if (isset($link['children']) && count($link['children']) > 0) {
				if (!isset($linkAttr['class'])) {
					$linkAttr['class'] = '';
				}
				$linkOutput = $this->Html->link($link['Link']['title'] . '<b class="caret"></b>', $link['Link']['link'], array('class' => $options['toggle'], 'data-toggle' => $options['dropdownClass'], 'escape' => false));
				$linkAttr['class'] .= ' ' . $options['dropdownClass'];
				$linkOutput .= $this->nestedLinks($link['children'], $options, $depth + 1);
			}
			$linkOutput = $this->Html->tag('li', $linkOutput, $linkAttr);
			$output .= $linkOutput;
		}
		if ($output != null) {
			$tagAttr = $options['tagAttributes'];
			if ($options['dropdown'] && $depth == 1) {
				$tagAttr['class'] = $options['menuClass'];
			}
			if ($depth > 1) {
				$tagAttr['class'] = $options['dropdownMenuClass'];
			}
			$output = $this->Html->tag($options['tag'], $output, $tagAttr);
		}

		return $output;
	}

	public function splitString($string, $number_character = 4, $symbol_char = '-')
	{
		$arr_string = str_split($string);
		$count = count($arr_string);
		$result = '';

		$idx = 0;
		foreach ($arr_string as $key => $value) {
			$idx++;
			if($idx % $number_character == 0 && $count != $idx){
				$result .= $symbol_char;
			}

			$result .= $value;
		}

		return $result;
	}

	public function tags($Taxonomy, $type)
	{
		$result = '';
		if(!empty($Taxonomy)){
			$arr = array();
			$count = count($Taxonomy);

			$idx = 1;
			foreach ($Taxonomy as $key => $val) {
				$title = $val['Term']['title'];
				$slug = $val['Term']['slug'];

				if($count != $idx++){
					$title .= ',';
				}

				$arr[] = $this->Html->link($title, array(
					'plugin' => 'nodes', 
					'controller' => 'nodes',
					'action' => 'term', 
					'type' => $type,
					'slug' => $slug
				));
			}

			$result = $this->Html->nestedList($arr, array(
				'class' => 'tags'
			));
		}

		return $result;
	}
}