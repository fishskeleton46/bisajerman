<?php
		$this->start('css');
		
		echo $this->Html->css(array(
			'style_login',
		));	

		$this->end();
?>
<section id="content">
	<?php echo $this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'forgot')));?>
		<h1><?php echo $title_for_layout; ?></h1>
		<div>
		<?php
			echo $this->Form->input('username', array('label' => false, 'placeholder' => 'Username'));
		?>
		</div>
	<?php echo $this->Form->end(__d('croogo', 'Submit'));?>
</section>
