<?php
		$this->start('css');
		
		echo $this->Html->css(array(
			'style_login',
		));	

		$this->end();
?>
<section id="content">
	<?php echo $this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'login')));?>
		<h1><?php echo $title_for_layout; ?></h1>
		<div>
			<?php
					echo $this->Form->input('username', array('label' => false, 'id' => 'username', 'placeholder' => 'Username'));
			?>
		</div>
		<div>
			<?php
					echo $this->Form->input('password', array('label' => false, 'id' => 'password', 'placeholder' => 'Password'));
			?>
		</div>
		<div>
			<?php
					echo $this->Form->submit(__d('croogo', 'Log In'), array(
						'div' => false,
						'class' => 'btn btn-success'
					));

					echo $this->Html->link(__d('croogo', 'Forgot password?'), array(
						'controller' => 'users', 'action' => 'forgot',
					));
			?>
		</div>
	<?php echo $this->Form->end(); ?>
</section><!-- content -->
