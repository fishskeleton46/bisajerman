<section class="blog-banner-area relative" id="home">
	<div class="overlay overlay-bg"></div>
	<div class="container">
		<div class="row fullscreen text-center d-flex align-items-center justify-content-center">
			<div class="banner-content col-lg-9 col-md-12">
				<h1 class="text-uppercase">Testimonial</h1>
				<p class="text-white">Banyak momen yang terekam saat kita bersama, inilah kata mereka</p>
			</div>
		</div>
	</div>
</section>
<section class="post-content-area">
	<div class="container">
		<div class="row">
			<?php
				if (count($nodes) == 0) {
					echo $this->Html->image('empty.png', array(
						'class' => 'empty-img img-thumbnail'
					));
				}

				foreach ($nodes as $node):
					$this->Nodes->set($node);

					$thumbnail = $this->Html->image('avatar-default.png', array(
						'class' => 'attachment-thumb_testimonial wp-post-image'
					));

					if(!empty($node['Nodeattachment'])){
						foreach ($node['Nodeattachment'] as $key => $value) {
							if($value['type'] == 'Main'){
								$image = $value;
							}
						}

						if(!empty($image)){
							$this->Nodeattachment->setNodeattachment($image);

							$thumbnail = $this->Image2->resize($this->Nodeattachment->field('thumb_path'), 94, 94, 'resizeRatio', 
								array(
									'alt' => $this->Nodeattachment->field('slug'),
									'class' => 'attachment-thumb_testimonial wp-post-image',
									'width' => 94,
									'height' => 94
								), 
								false, $this->Nodeattachment->field('server_thumb_path'));
						}
					}
			?>
			<div class="col-md-6">
				<div class="testimonial-content">
					<div class="thumbnail"> 
						<?php echo $thumbnail;?>
					</div>
					<div class="testimonial-content-text">
						<?php
								echo $this->Html->tag('p', $this->Nodes->field('body'));
						?>
					</div>
					<div class="testimonial-content-name">
						<?php
								echo $this->Html->tag('p', $this->Nodes->field('title'), array(
									'class' => 'name'
								));
						?>
					</div>
				</div>
			</div>
			
			<?php 
					endforeach;
			?>
			
		</div>
		<?php
				echo $this->element('pagination');
		?>
	</div>
</section>