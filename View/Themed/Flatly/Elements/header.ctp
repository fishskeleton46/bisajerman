<?php
		$facebook 	= Configure::read('SocialMedia.facebook');
		$twitter 	= Configure::read('SocialMedia.twitter');
		$instagram 	= Configure::read('SocialMedia.instagram');

		$phone 	= Configure::read('Site.phone');
		$email 	= Configure::read('Site.email');
?>
<header id="header" id="home">
	<div class="header-top">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-sm-6 col-4 no-padding">
					<?php
							if(!empty($phone)){
					?>
					<div class="header-top-left">
						<a href="tel:<?php echo $phone;?>">
							<i class="fa fa-phone"></i>
							<?php echo $this->Custom->splitString($phone);?>
						</a>
						<a href="mail:<?php echo $email;?>">
							<i class="fa fa-email"></i>
							<?php echo $email;?>
						</a>
					</div>
					<?php
							}
					?>
				</div>
				<div class="col-lg-6 col-sm-6 col-8 header-top-right no-padding">
					<ul>
						<?php
								if(!empty($facebook)){
						?>
						<li><a href="<?php echo $facebook;?>"><i class="fa fa-facebook"></i></a></li>
						<?php
								}

								if(!empty($twitter)){
						?>
						<li><a href="<?php echo $twitter;?>"><i class="fa fa-twitter"></i></a></li>
						<?php
								}

								if(!empty($instagram)){
						?>
						<li><a href="<?php echo $instagram;?>"><i class="fa fa-instagram"></i></a></li>
						<?php
								}
						?>
						<?php if ($this->Session->read('Auth.User.id')): ?>
						<li>
							<?php 
									echo $this->Html->link('<i class="fa fa-sign-out"></i>', array(
										'plugin' => 'users', 
										'controller' => 'users', 
										'action' => 'logout'
									), array(
										'escape' => false,
										'title' => __('Log out')
									)); 
							?>
						</li>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<hr>
	<div class="container main-menu">
		<div class="row align-items-center justify-content-between d-flex">
			<div id="logo">
				<a href="/" style="font-size: 30px;">
					<i class="fa fa-comment" style="color: #ccc;"></i> <span style="color: #DC0000;">BISA</span><span style="color: #FECE00;">JERMAN</span>
				</a>
			</div>
			<nav id="nav-menu-container">
				<?php
					echo $this->Custom->menu('main', array(
						'dropdown' => true,
						'menuClass' => 'nav-menu',
						'selected' => 'menu-active',
						'dropdownMenuClass' => '',
						'dropdownClass' => 'menu-has-children',
						'toggle' => ''
					));
				?>
			</nav>
			<!--######## #nav-menu-container -->
		</div>
	</div>
</header>