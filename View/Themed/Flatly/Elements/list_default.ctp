<section class="post-content-area mt-30">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 posts-list">
				<?php
						if (count($nodes) == 0) {
							echo $this->Html->image('empty.png', array(
								'class' => 'empty-img img-thumbnail'
							));
						}

						foreach ($nodes as $node):
							$this->Nodes->set($node);

							$excerpt 		= $this->Nodes->field('excerpt');
							$created 		= $this->Nodes->field('created');
							$comment_count 	= $this->Nodes->field('comment_count');
							$type 			= $this->Nodes->field('type');

							$name = $node['User']['name'];

							$thumbnail = '';
							if(!empty($node['Nodeattachment'])){
								foreach ($node['Nodeattachment'] as $key => $value) {
									if($value['type'] == 'Main'){
										$image = $value;
									}
								}

								if(!empty($image)){
									$this->Nodeattachment->setNodeattachment($image);

									$thumbnail = $this->Image2->resize($this->Nodeattachment->field('thumb_path'), 555, 280, 'resizeRatio', 
										array(
											'alt' => $this->Nodeattachment->field('slug'),
											'class' => 'img-fluid'
										), 
										false, $this->Nodeattachment->field('server_thumb_path'));
								}
							}
				?>
				<div class="single-post row">
					<div class="col-lg-3  col-md-3 meta-details">
						<?php
								echo $this->Custom->tags($node['Taxonomy'], $type);
						?>
						<div class="user-details row">
							<p class="user-name col-lg-12 col-md-12 col-6"><a href="javascript:void(0)"><?php echo $name;?></a> <span class="lnr lnr-user"></span></p>
							<p class="date col-lg-12 col-md-12 col-6"><a href="javascript:void(0)"><?php echo $this->Time->format('d M, Y', $created);?></a> <span class="lnr lnr-calendar-full"></span></p>
							<p class="comments col-lg-12 col-md-12 col-6"><a href="javascript:void(0)"><?php echo $comment_count.' Comments';?></a> <span class="lnr lnr-bubble"></span></p>
						</div>
					</div>
					<div class="col-lg-9 col-md-9 ">
						<?php
								if(!empty($thumbnail)){
						?>
						<div class="feature-img">
							<?php echo $thumbnail;?>
						</div>
						<?php
								}
						?>
						<?php 
								echo $this->Html->link(
									$this->Html->tag('h3', $this->Nodes->field('title')), 
									$this->Nodes->field('url'),
									array(
										'escape' => false,
										'class' => 'posts-title'
									)
								); 

								if(!empty($excerpt)){
									$text = $excerpt;
								}else{
									$text = $this->Text->truncate(strip_tags($this->Nodes->field('body')), 300, array(
								        'ellipsis' => '...',
								        'exact' => false
								    ));
								}

								echo $this->Html->tag('p', $text, array(
									'class' => 'excert'
								));

								echo $this->Html->link(__('Selengkapnya'), $this->Nodes->field('url'), array(
									'class' => 'primary-btn'
								));
						?>
					</div>
				</div>
				<?php 
						endforeach;

						echo $this->element('pagination');
				?>
			</div>
			<div class="col-lg-4 sidebar-widgets">
				<?php echo $this->element('sidebar');?>
			</div>
		</div>
	</div>
</section>