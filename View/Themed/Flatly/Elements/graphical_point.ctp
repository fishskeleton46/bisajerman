<?php
		$experiences 	= Configure::read('Miscellaneous.experiences');
		$students 		= Configure::read('Miscellaneous.students');
		$reason 		= Configure::read('Miscellaneous.reason');
?>
<!--######## Start Our Offer Area ########-->
<section class="our-offer-area section-gap">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg-6 col-md-6 mb-30">
						<div class="single-circle">
							<div class="single-item">
								<div class="progressBar progressBar--animateText" data-progress="75">
									<svg class="progressBar-contentCircle" viewBox="0 0 200 200">
										<circle transform="rotate(-90, 100, 100)" class="progressBar-background" cx="100" cy="100" r="95" />
										<circle transform="rotate(-90, 100, 100)" class="progressBar-circle" cx="100" cy="100" r="95" />
									</svg>
									<span class="progressBar-percentage progressBar-percentage-count"><?php echo $students;?></span>
								</div>
							</div>
							<h4>Happy Students</h4>
						</div>
					</div>

					<div class="col-lg-6 col-md-6 mb-30">
						<div class="single-circle">
							<div class="single-item">
								<div class="progressBar progressBar--animateText" data-progress="75">
									<svg class="progressBar-contentCircle" viewBox="0 0 200 200">
										<circle transform="rotate(-90, 100, 100)" class="progressBar-background" cx="100" cy="100" r="95" />
										<circle transform="rotate(-90, 100, 100)" class="progressBar-circle" cx="100" cy="100" r="95" />
									</svg>
									<span class="progressBar-percentage progressBar-percentage-count"><?php echo $experiences;?></span>
								</div>
							</div>
							<h4>Years of Experience</h4>
						</div>
					</div>
				</div>
			</div>

			<div class="offset-lg-1 col-lg-5">
				<div class="row justify-content-center">
					<div class="col-lg-12">
						<div class="main-title text-left">
							<h1>Kami adalah Solusi Anda</h1>
							<p><?php echo $reason;?></p>
							<a href="#" class="primary-btn offer-btn mr-10">Kami menawarkan</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--######## End Our Offer Area ########-->