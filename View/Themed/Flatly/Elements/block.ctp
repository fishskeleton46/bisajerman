<?php
$this->set(compact('block'));
$b = $block['Block'];
$class = 'block block-' . $b['alias'];
if ($block['Block']['class'] != null) {
	$class .= ' ' . $b['class'];
}
?>
<div id="block-<?php echo $b['id']; ?>" class="single-sidebar-widget <?php echo $class; ?>">
<?php if ($b['show_title'] == 1): ?>
	<h4 class="category-title"><?php echo $b['title']; ?></h4>
<?php endif; ?>
<?php
	echo $this->Layout->filter($b['body'], array(
		'model' => 'Block', 'id' => $b['id']
	));
?>
</div>