<?php
		$options = array(
			'tag' => 'li',
			'escape' => false,
			'class' => 'page-item'
		);
?>
<nav class="blog-pagination justify-content-center d-flex">
	<ul class="pagination">
		<?php
				if($this->Paginator->hasPrev()){
					echo $this->Paginator->prev('<span aria-hidden="true"><span class="lnr lnr-chevron-left"></span></span>', $options);
				}

				echo $this->Paginator->numbers(array(
					'currentTag' => 'a',
					'tag' => 'li',
					'separator' => false,
					'currentClass' => 'page-item active',
					'escape' => false
				));

				if($this->Paginator->hasNext()){
					echo $this->Paginator->next('<span aria-hidden="true"><span class="lnr lnr-chevron-right"></span></span>', $options);
				}
		?>
	</ul>
</nav>