<?php
		if(!empty($testimonials)){
?>
<section class="testimonial-area section-gap">
	<div class="container">
		<div class="row">
			<div class="active-testimonial-carusel">
				<?php
						foreach ($testimonials as $key => $node) {
							$this->Nodes->set($node);

							$thumbnail = $this->Html->image('avatar-default.png', array(
								'class' => 'img-fluid'
							));

							if(!empty($node['Nodeattachment'])){
								foreach ($node['Nodeattachment'] as $key => $value) {
									if($value['type'] == 'Main'){
										$image = $value;
									}
								}

								if(!empty($image)){
									$this->Nodeattachment->setNodeattachment($image);

									$thumbnail = $this->Image2->resize($this->Nodeattachment->field('thumb_path'), 263, 180, 'resizeRatio', 
										array(
											'alt' => $this->Nodeattachment->field('slug'),
											'class' => 'img-fluid'
										), 
										false, $this->Nodeattachment->field('server_thumb_path'));
								}
							}
				?>
				<div class="single-testimonial item d-flex flex-row">
					<?php
							if(!empty($thumbnail)){
								echo $this->Html->div('thumb', $thumbnail);
							}
					?>
					<div class="desc">
						<?php
								echo $this->Html->tag('p', $this->Nodes->field('body'));
								echo $this->Html->tag('h4', $this->Nodes->field('title'), array(
									'class' => 'mt-30'
								));
						?>
					</div>
				</div>
				<?php
						}
				?>
			</div>
		</div>
	</div>
</section>
<?php
		}
?>