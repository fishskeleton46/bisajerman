<?php
		$crumbs = $this->Html->getCrumbs(
			$this->Html->tag('span', '', array(
				'class' => 'lnr lnr-arrow-right',
			))
		);

		if ($crumbs): 
?>
<section class="banner-area relative" id="home">
	<div class="container">
		<div class="row d-flex align-items-center justify-content-center">
			<div class="about-content col-lg-12">
				<h1 class="text-uppercase"><?php echo $title_for_layout;?></h1>
				<p class="link-nav">
					<a href="/">Home </a>
					<?php 
							echo $this->Html->tag('span', '', array(
								'class' => 'lnr lnr-arrow-right',
							));

							echo $crumbs; 
					?>
				</p>
			</div>
		</div>
	</div>
</section>
<?php 
		endif; 
?>
