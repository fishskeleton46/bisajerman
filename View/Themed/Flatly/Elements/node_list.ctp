<div class="popular-post-list">
	<?php
			foreach ($nodesList as $node) {
				$created = $node['Node']['created'];
				$created = $this->Time->format($created, '%B %e, %Y %H:%M %p');

				$thumbnail = '';
				if(!empty($node['Nodeattachment'])){
					foreach ($node['Nodeattachment'] as $key => $value) {
						if($value['type'] == 'Main'){
							$image = $value;
						}
					}

					if(!empty($image)){
						$this->Nodeattachment->setNodeattachment($image);

						$thumbnail = $this->Image2->resize($this->Nodeattachment->field('thumb_path'), 100, 60, 'resizeRatio', 
							array(
								'alt' => $this->Nodeattachment->field('slug'),
								'class' => 'img-fluid'
							), 
							false, $this->Nodeattachment->field('server_thumb_path'));
					}
				}
	?>
	<div class="single-post-list d-flex flex-row align-items-center">
		<?php
				if(!empty($thumbnail)){
					echo $this->Html->div('thumb', $thumbnail);
				}
		?>
		<div class="details">
			<?php
					if ($options['link']) {
						echo $this->Html->link($this->Html->tag('h6', $node['Node']['title']), array(
							'plugin' => $options['plugin'],
							'controller' => $options['controller'],
							'action' => $options['action'],
							'type' => $node['Node']['type'],
							'slug' => $node['Node']['slug'],
						), array(
							'escape' => false
						));
					} else {
						echo '<h6>' . $node['Node']['title'] . '</h6>';
					}

					echo $this->Html->tag('p', $created);
			?>
		</div>
	</div>
	<?php
			}
	?>
</div>