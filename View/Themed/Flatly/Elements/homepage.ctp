<?php
		$this->start('css');
		
		echo $this->Html->css(array(
			'https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.11/css/lightgallery.min.css',
		));

		$this->end();
?>
<section class="section-gap home-banner-area relative" id="home">
	<div class="container">
		<div class="row fullscreen d-flex align-items-center">
			<div class="banner-content col-md-12">
				<h1>Yuk <span style="color: #DC0000;">Bisa</span> <span style="color: #FECE00;">Jerman</span> . . . ! ! !</h1>
				<a href="contact/contact" class="primary-btn header-btn text-capitalize mt-10">Hubungi Kami Sekarang!</a>
			</div>
		</div>
	</div>
</section>

<?php
		echo $this->element('blog_promoted');
		echo $this->element('graphical_point');
		echo $this->element('team');
		echo $this->element('testimonials');
?>