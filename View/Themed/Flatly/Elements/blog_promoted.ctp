<?php
		if(!empty($nodes)){
?>
<section class="single-element-widget latest-blog-area">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-12">
				<div class="main-title text-center">
					<h1><?php echo __('Terbaru dari Kami');?></h1>
					<p><?php echo __('Kami berusaha menyajikan info dari segala berita tentang jerman');?></p>
				</div>
			</div>
		</div>

		<div class="row">
			<?php
					foreach ($nodes as $key => $node) {
						$this->Nodes->set($node);

						$created 		= $this->Time->format('d M, Y', $this->Nodes->field('created'));
						$comment_count 	= $this->Nodes->field('comment_count');

						$thumbnail = '';
						if(!empty($node['Nodeattachment'])){
							foreach ($node['Nodeattachment'] as $key => $value) {
								if($value['type'] == 'Main'){
									$image = $value;
								}
							}

							if(!empty($image)){
								$this->Nodeattachment->setNodeattachment($image);

								$thumbnail = $this->Image2->resize($this->Nodeattachment->field('thumb_path'), 263, 180, 'resizeRatio', 
									array(
										'alt' => $this->Nodeattachment->field('slug'),
										'class' => 'img-fluid w-100'
									), 
									false, $this->Nodeattachment->field('server_thumb_path'));
							}
						}
			?>
			<div class="col-lg-3 col-md-6 single-blog">
				<?php
						if(!empty($thumbnail)){
							echo $this->Html->div('thumb', $thumbnail);
						}
				?>
				<p class="date"><?php echo $created;?></p>
				<?php
						echo $this->Html->tag('h4', $this->Html->link($this->Nodes->field('title'), $this->Nodes->field('url')));

						echo $this->Html->tag('p', $this->Text->truncate($this->Nodes->field('excerpt')));
				?>
				<div class="meta-bottom d-flex justify-content-between">
					<p><span class="lnr lnr-bubble"></span> <?php echo $comment_count;?> Comments</p>
				</div>
			</div>
			<?php
					}
			?>
		</div>
	</div>
</section>
<?php
		}
?>