<?php
		$facebook 	= Configure::read('SocialMedia.facebook');
		$twitter 	= Configure::read('SocialMedia.twitter');
		$instagram 	= Configure::read('SocialMedia.instagram');
?>
<footer class="footer-area section-gap">
	<div class="container">
		<div class="row ">
			<div class="col-lg-3 col-md-6 ">
				<div class="single-footer-widget ">
					<h6>Info</h6>
					<?php
						echo $this->Custom->menu('footer', array(
							'dropdown' => true,
							'menuClass' => 'footer-nav',
						));
					?>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 ">
				<div class="single-footer-widget newsletter ">
					<h6>Pencarian</h6>
					<p>Kamu bisa mencari konten atau kursus yang kamu butuhkan</p>
					<div id="mc_embed_signup ">
						<?php
								echo $this->Form->create('Node', array(
									'url' => array(
										'admin' => false, 
										'plugin' => 'nodes', 
										'controller' => 'nodes', 
										'action' => 'search', 
										'class' => 'form-search'
									),
									'class' => 'form-inline',
								));
						?>
							<div class="form-group row " style="width: 100% ">
								<div class="col-lg-8 col-md-12 ">
									<?php
											$this->Form->unlockField('q');

											echo $this->Form->input('q', array(
												'label' => false,
												'placeholder' => __('Cari'),
												'onfocus' => "this.placeholder = ''",
												'onblur' => "this.placeholder = 'Cari'",
												'div' => false
											));
									?>
								</div>

								<div class="col-lg-4 col-md-12 ">
									<button class="nw-btn primary-btn" type="submit">Cari<span class="lnr lnr-arrow-right "></span></button>
								</div>
							</div>
							<div class="info "></div>
						<?php echo $this->Form->end();?>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-bottom d-flex justify-content-between align-items-center flex-wrap ">
			<p class="col-lg-8 col-sm-12 footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
			&copy;<script>document.write(new Date().getFullYear());</script> bisajerman.com - <span class="text-white">by fishskeleton46</span>
			<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
			</p>
			<div class="footer-social d-flex align-items-center">
				<?php
						if(!empty($facebook)){
				?>
				<a href="<?php echo $facebook;?>"><i class="fa fa-facebook"></i></a>
				<?php
						}

						if(!empty($twitter)){
				?>
				<a href="<?php echo $twitter;?>"><i class="fa fa-twitter"></i></a>
				<?php
						}

						if(!empty($instagram)){
				?>
				<a href="<?php echo $instagram;?>"><i class="fa fa-instagram"></i></a>
				<?php
						}
				?>
			</div>
		</div>
	</div>
</footer>