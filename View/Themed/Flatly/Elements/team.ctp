<section class="team section-gap">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="main-title text-center">
					<h1>Pengajar Terbaik Kami</h1>
					<p>Kami memiliki pengajar-pengajar hebat dengan pengalaman lebih dari 5 tahun di bidangnya.</p>
				</div>
			</div>
		</div>
		<div class="row team_row">
			
			<!-- Team Item -->
			<div class="col-lg-6 col-md-6 team_col">
				<div class="team_item">
					<div class="team_image"><?php echo $this->Html->image('ervina.png');?></div>
					<div class="team_body">
						<div>Ervina Oktavitri</div>
						<div>Saya memiliki pengalaman lebih dari 5 tahun</div>
						<div class="social_list">
							<ul>
								<li><a href="https://www.facebook.com/ervina.oktavitri" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="https://www.linkedin.com/in/ervina-oktavitri-5697b6b0/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
								<li><a href="https://www.instagram.com/rvinaokt/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-6 col-md-6 team_col">
				<div class="team_item">
					<div class="team_image"><?php echo $this->Html->image('barlian.jpeg');?></div>
					<div class="team_body">
						<div>Dipl.-Ing(Univ) Barlian Sapta</div>
						<div>Pengasuh dan pengajar di Saptastudy</div>
						<div class="social_list">
							<ul>
								<li><a href="http://www.saptastudy.com" target="_blank"><i class="fa fa-link" aria-hidden="true"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
