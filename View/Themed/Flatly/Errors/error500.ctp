<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Errors
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<?php
if (Configure::read('debug') > 0):
	echo $this->element('exception_stack_trace');	
?>
	<h2><?php echo $message; ?></h2>
	<p class="error">
		<strong><?php echo __d('cake', 'Error'); ?>: </strong>
		<?php echo __d('cake', 'An Internal Error Has Occurred.'); ?>
	</p>
<?php
	else:
		$this->start('css');
			echo $this->Html->css(array(
				'style_error',
			));
		$this->end();
?>
		<div id="notfound">
			<div class="notfound">
				<div class="notfound-404">
					<h1>500</h1>
				</div>
				<h2>Oops! Something wrong</h2>
				<p>Sorry, but the page you requested is experiencing some constraints, please come back later</p>
			</div>
		</div>
<?php
	endif;
?>

